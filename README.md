# LoaRdfPubAdapter

A library, that supports functionality needed for [linkedopenactors](https://linkedopenactors.org/) apps.  
It's an abstraction with convenience methods for using [rdf-pub](https://linkedopenactors.gitlab.io/rdf-pub/).

See: [LoaRdfPubAdapter](https://gitlab.com/linkedopenactors/loa-rdfpub-adapter/-/blob/develop/loa-rdfpub-adapter-lib/src/main/java/org/linkedopenactors/loardfpubadapter/LoaRdfPubAdapter.java)
