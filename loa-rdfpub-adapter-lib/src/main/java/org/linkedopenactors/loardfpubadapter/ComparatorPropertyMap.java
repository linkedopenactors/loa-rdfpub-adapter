package org.linkedopenactors.loardfpubadapter;

import java.util.List;

import de.danielbechler.diff.ObjectDifferBuilder;
import de.danielbechler.diff.node.DiffNode;
import de.danielbechler.diff.node.Visit;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ComparatorPropertyMap {

	public static boolean hasChanges(String propertyMapType, List<String> oldPropertyMap, List<String> newPropertyMap) {
		DiffNode diff = ObjectDifferBuilder.buildDefault().compare(newPropertyMap, oldPropertyMap);
		log.trace("analysis of '" + propertyMapType + "'");
		log.trace("\thasChanges() : " + diff.hasChanges());
		traceDiff(diff);
		return diff.hasChanges();
	}
	
	private static void traceDiff(DiffNode diff) {
		diff.visit(new DiffNode.Visitor()
		{
		    public void node(DiffNode node, Visit visit)
		    {
		    	log.trace("\t" + node.getPath() + " => " + node.getState());
		    }
		});
	}
}
