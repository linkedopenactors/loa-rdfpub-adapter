package org.linkedopenactors.loardfpubadapter;

import java.util.List;

import org.linkedopenactors.loardfpubadapter.model.Publication;

import de.naturzukunft.rdf4j.vocabulary.AS;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

public class ComparatorPlace {
	public static boolean hasIdenticalPlace(Publication oldPublication, Publication newPublication) {
		List<String> oldPropertyMap = oldPublication.getOrgansation().getLocation().orElseThrow().getPropertyMap(SCHEMA_ORG.address, AS.published, AS.attributedTo);
		List<String> newPropertyMap = newPublication.getOrgansation().getLocation().orElseThrow().getPropertyMap(SCHEMA_ORG.address, AS.published, AS.attributedTo);
		return !ComparatorPropertyMap.hasChanges(ComparatorPlace.class.getSimpleName(), oldPropertyMap, newPropertyMap);
	}
}
