package org.linkedopenactors.loardfpubadapter;

import org.linkedopenactors.loardfpubadapter.model.Publication;

/**
 * Ensures, that the version of the publicationToCreate is correct.
 * Correct means '0' if there is not already a publication with the same identifier or the version of the existing publication incremented by one.   
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
public class PublicationVersionIncrementChecker {

	/**
	 * Ensures, that the version of the publicationToCreate is correct.
	 * Correct means '0' if there is not already a publication with the same identifier or the version of the existing publication incremented by one. 
	 * @param publicationToCreate The publication that should be created
	 * @param existingPublicationVersion The version ofthe existing publication with the same identifier than the publicationToCreate, or null if there is no publication for that identifier.
	 * @throws IllegalStateException if there is a version mismatch.   
	 */
	public static void checkOrSet(Publication publicationToCreate, Integer existingPublicationVersion) {
		checkIfPassedVersionIsOneHigherThanTheNewestPublicationVersion(publicationToCreate.getVersion(), existingPublicationVersion);
	}
	
	/**
	 * Checks whether the version passed is one higher than the version of the newest publication in the store.  
	 * @param version the version to check.
	 * @param existingPublication the newest publication in the store.
	 * @throws IllegalStateException if there is a version mismatch. 
	 */
	private static void checkIfPassedVersionIsOneHigherThanTheNewestPublicationVersion(Integer version, Integer existingVersion) {
		if (existingVersion != null) {
			if (version - 1 != existingVersion) {
				
				AlreadyExistingVersionException alreadyExistingVersionException = new AlreadyExistingVersionException(
						"Passed version '" + version + "' has to be " + (existingVersion + 1)
								+ ", because the newest publication in the store is " + existingVersion);
				alreadyExistingVersionException.setExistingPublicationVersion(existingVersion);
				alreadyExistingVersionException.setVersionToAdd(version);
				throw alreadyExistingVersionException;
			}
		}
	}
}
