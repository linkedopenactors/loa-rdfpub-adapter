package org.linkedopenactors.loardfpubadapter;

import java.util.List;

import org.linkedopenactors.loardfpubadapter.model.Publication;

import de.naturzukunft.rdf4j.vocabulary.AS;

public class ComparatorPostalAddress {
	public static boolean hasIdenticalPostalAddress(Publication oldPublication, Publication newPublication) {
		List<String> oldPropertyMap = oldPublication.getOrgansation().getLocation().orElseThrow().getPostalAddress().orElseThrow().getPropertyMap(AS.published, AS.attributedTo);
		List<String> newPropertyMap = newPublication.getOrgansation().getLocation().orElseThrow().getPostalAddress().orElseThrow().getPropertyMap(AS.published, AS.attributedTo);
		return !ComparatorPropertyMap.hasChanges(ComparatorPostalAddress.class.getSimpleName(), oldPropertyMap, newPropertyMap);		
	}
}
