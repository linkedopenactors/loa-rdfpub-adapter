package org.linkedopenactors.loardfpubadapter;

import static org.linkedopenactors.loardfpubadapter.ComparatorContactPoint.hasIdenticalContactPoint;
import static org.linkedopenactors.loardfpubadapter.ComparatorPlace.hasIdenticalPlace;
import static org.linkedopenactors.loardfpubadapter.ComparatorPostalAddress.hasIdenticalPostalAddress;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.linkedopenactors.loardfpubadapter.model.Publication;
import org.linkedopenactors.loardfpubadapter.model.PublicationModelsFactory;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

public class CreatePublicationMonitor {

	public Publication monitorCreation(Publication toCreatePublication, Publication existingPublication) {
		boolean hasIdenticalPostalAddress = hasIdenticalPostalAddress(existingPublication, toCreatePublication);
		boolean hasIdenticalPlace = hasIdenticalPlace(existingPublication, toCreatePublication);
		boolean hasIdenticalContactPoint = hasIdenticalContactPoint(existingPublication, toCreatePublication);
		boolean hasIdenticalOrganisation = ComparatorOrganisation.hasIdenticalOrganisation(existingPublication, toCreatePublication);
		
		if(hasIdenticalOrganisation && !hasIdenticalPlace && !hasIdenticalContactPoint && !hasIdenticalPostalAddress) {
			toCreatePublication = linkOrganisationOfCreativeWorkToExistingOrganisation(toCreatePublication, existingPublication);
		}
		if(hasIdenticalPostalAddress) {
			toCreatePublication = linkPostalAddressOfPlaceToExistingPostalAddress(toCreatePublication, existingPublication);
		}				
		if(hasIdenticalPlace && hasIdenticalPostalAddress) {
			toCreatePublication = linkLocationOfOrganisationToExistingPlace(toCreatePublication, existingPublication);
		}		
		if(hasIdenticalContactPoint) {
			toCreatePublication = linkContactPointOfOrganisationToExistingContactPonit(toCreatePublication, existingPublication);
		}			
		return toCreatePublication;
	}

	private Publication linkOrganisationOfCreativeWorkToExistingOrganisation(Publication toCreatePublication, Publication existingPublication) {
		IRI toCreatePublicationOrganisationSubject = toCreatePublication.getOrgansation().getId();
		IRI existingPublicationOrganisationSubject = existingPublication.getOrgansation().getId();
		Model toCreatePublicationModel = clone(toCreatePublication.getModel());
		toCreatePublicationModel.removeAll(toCreatePublication.getModel().filter(toCreatePublicationOrganisationSubject, null, null));				
		toCreatePublicationModel.remove(toCreatePublication.getId(), SCHEMA_ORG.about, null);				
		toCreatePublicationModel.add(toCreatePublication.getId(), SCHEMA_ORG.about, existingPublicationOrganisationSubject);
		
		return PublicationModelsFactory.getPublicationInstance(new ModelAndSubject(toCreatePublication.getId(), toCreatePublicationModel));
	}
	
	private Publication linkLocationOfOrganisationToExistingPlace(Publication toCreatePublication, Publication existingPublication) {
		IRI toCreatePublicationPlaceSubject = toCreatePublication.getOrgansation().getLocation().orElseThrow().getId();
		IRI existingPublicationPlaceSubject = existingPublication.getOrgansation().getLocation().orElseThrow().getId();
		Model toCreatePublicationModel = clone(toCreatePublication.getModel());
		toCreatePublicationModel.removeAll(toCreatePublication.getModel().filter(toCreatePublicationPlaceSubject, null, null));				
		toCreatePublicationModel.remove(toCreatePublication.getOrgansation().getId(), SCHEMA_ORG.location, null);				
		toCreatePublicationModel.add(toCreatePublication.getOrgansation().getId(), SCHEMA_ORG.location, existingPublicationPlaceSubject);
		
		return PublicationModelsFactory.getPublicationInstance(new ModelAndSubject(toCreatePublication.getId(), toCreatePublicationModel));
	}

	private Publication linkPostalAddressOfPlaceToExistingPostalAddress(Publication toCreatePublication, Publication existingPublication) {
		IRI toCreatePublicationPostalAddressSubject = toCreatePublication.getOrgansation().getLocation().orElseThrow().getPostalAddress().orElseThrow().getId();
		IRI existingPublicationPostalAddressSubject = existingPublication.getOrgansation().getLocation().orElseThrow().getPostalAddress().orElseThrow().getId();
		Model toCreatePublicationModel = clone(toCreatePublication.getModel());
		toCreatePublicationModel.removeAll(toCreatePublication.getModel().filter(toCreatePublicationPostalAddressSubject, null, null));				
		toCreatePublicationModel.remove(toCreatePublication.getOrgansation().getLocation().orElseThrow().getId(), SCHEMA_ORG.address, null);				
		toCreatePublicationModel.add(toCreatePublication.getOrgansation().getLocation().orElseThrow().getId(), SCHEMA_ORG.address, existingPublicationPostalAddressSubject);
		
		return PublicationModelsFactory.getPublicationInstance(new ModelAndSubject(toCreatePublication.getId(), toCreatePublicationModel));
	}

	private Publication linkContactPointOfOrganisationToExistingContactPonit(Publication toCreatePublication, Publication existingPublication) {
		
		IRI toCreatePublicationContactPointSubject = toCreatePublication.getOrgansation().getContactPoint().orElseThrow().getId();
		IRI existingPublicationContactPointSubject = existingPublication.getOrgansation().getContactPoint().orElseThrow().getId();
		
		Model toCreatePublicationModel = clone(toCreatePublication.getModel());
		toCreatePublicationModel.removeAll(toCreatePublication.getModel().filter(toCreatePublicationContactPointSubject, null, null));				
		toCreatePublicationModel.remove(toCreatePublication.getOrgansation().getId(), SCHEMA_ORG.contactPoint, null);				
		toCreatePublicationModel.add(toCreatePublication.getOrgansation().getId(), SCHEMA_ORG.contactPoint, existingPublicationContactPointSubject);
		
		return PublicationModelsFactory.getPublicationInstance(new ModelAndSubject(toCreatePublication.getId(), toCreatePublicationModel));
	}

	private Model clone(Model toCreatePublicationModel) {
		Model clone = new ModelBuilder().build();
		clone.addAll(toCreatePublicationModel);
		return clone;
	}
}
