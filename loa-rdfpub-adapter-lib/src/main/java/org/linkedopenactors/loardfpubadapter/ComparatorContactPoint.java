package org.linkedopenactors.loardfpubadapter;

import java.util.List;

import org.linkedopenactors.loardfpubadapter.model.Publication;

import de.naturzukunft.rdf4j.vocabulary.AS;

public class ComparatorContactPoint {
	public static boolean hasIdenticalContactPoint(Publication oldPublication, Publication newPublication) {
		List<String> oldPropertyMap = oldPublication.getOrgansation().getContactPoint().orElseThrow().getPropertyMap(AS.published, AS.attributedTo);
		List<String> newPropertyMap = newPublication.getOrgansation().getContactPoint().orElseThrow().getPropertyMap(AS.published, AS.attributedTo);
		return !ComparatorPropertyMap.hasChanges(ComparatorContactPoint.class.getSimpleName(), oldPropertyMap, newPropertyMap);
	}
}
