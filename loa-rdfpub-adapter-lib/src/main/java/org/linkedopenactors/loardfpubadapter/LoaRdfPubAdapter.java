package org.linkedopenactors.loardfpubadapter;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.query.BindingSet;
import org.linkedopenactors.loardfpubadapter.model.Publication;
import org.linkedopenactors.loardfpubadapter.model.PublicationHistory;
import org.linkedopenactors.loardfpubadapter.model.PublicationModelsFactory;
import org.linkedopenactors.rdfpub.client.RdfPubClient;
import org.linkedopenactors.rdfpub.client.SimpleProfile;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.utils.ModelLogger;
import de.naturzukunft.rdf4j.vocabulary.AS;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;
import lombok.extern.slf4j.Slf4j;

/**
 * {@link LoaRdfPubAdapter} supports functionality needed for <a href="https://linkedopenactors.org">linkedopenactors</a> apps.
 * It's an abstraction with convenience methods for using <a href="https://linkedopenactors.gitlab.io/rdf-pub/">rdf-pub</a>.
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Slf4j
public class LoaRdfPubAdapter {

	private RdfPubClient rdfPubClient;
	private SimpleProfile profile;
	
	public LoaRdfPubAdapter(RdfPubClient rdfPubClient) {
		this.rdfPubClient = rdfPubClient;
	}

	/**
	 * Gets a set of <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a> versions for the given identifier.
	 * @param identifier The identifier of the <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a>
	 * @return A set of <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a> versions for the given identifier.
	 */
	public Set<Integer> getVersions(String identifier, String authToken) {
		String query = """
				PREFIX schema: <https://schema.org/> 
				PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
				PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
				SELECT DISTINCT ?identifier ?version
				WHERE { 
				  		?subject rdf:type schema:CreativeWork .  	
				  		?subject schema:identifier "%s" .
				  		?subject schema:version ?version .  		
				  }	
				""".formatted(identifier);
		
		Set<Integer> versions = new HashSet<>();
		
		SimpleProfile profile = getSimpleProfile();
		List<BindingSet> bindingSets = profile.queryOutbox(query, authToken);
		bindingSets.stream().map(bs->bs.getValue("version")).forEach(version->{
	         if(version.isLiteral()) {
	        	 versions.add(((Literal)version).intValue());
	         }
		});
		return versions;
	}

	/**
	 * Gets the latest/biggest <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a> version for the given identifier.
	 * @param identifier The identifier of the <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a>
	 * @return The latest/biggest <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a> version for the given identifier.
	 */
	public Optional<Integer> getLatestVersion(String identifier, String authToken) {
		return getVersions(identifier, authToken).stream().max(Comparator.naturalOrder());
	}

	/**
	 * Gets the {@link PublicationHistory} for the given identifier.
	 * @param identifier The identifier of the <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a>
	 * @return The {@link PublicationHistory} for the given identifier.
	 */
	public Optional<PublicationHistory> getConstructedPublicationHistory(String identifier, String authToken) {
		Function<String, Model> queryable = query -> rdfPubClient.graphQueryOutbox(query, authToken);
		return new PublicationHistoryConstructor()
				.getConstructedPublication(queryable, getSimpleProfile().getActorId(), identifier);
	}

	/**
	 * Gets the {@link Publication} for the given identifier and the given version.
	 * @param identifier The identifier of the <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a>
	 * @param version The version of the wanted <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a>
	 * @return The {@link Publication} for the given identifier and the given version. 
	 */
	public Optional<Publication> getConstructedPublication(String identifier, long version, String authToken) {
		Function<String, Model> queryable = query -> rdfPubClient.graphQueryOutbox(query, authToken);
		return new PublicationHistoryConstructor()
				.getConstructedPublication(queryable, getSimpleProfile().getActorId(), identifier, version);
	}

	/**
	 * Checks, if there is already an object with the passed identifier. 
	 * @param identifier the {@link SCHEMA_ORG#identifier} of the object that have to be checked.
	 * @return True, if there is an object with that id, otherwise false.
	 */
	public boolean existsByIdentifier(String identifier, String authToken) {
		Map<String, IRI> versionSubjectPairs = determineSubjectsByIdentifier(identifier, authToken);
		if(versionSubjectPairs.size()> 1) {
			log.warn("There are more than on entry for identifier '"+identifier+"'" + versionSubjectPairs);
		}
		return !versionSubjectPairs.isEmpty();		
	}

	/**
	 * Determines the Subjects of the publications behind an identifier.
	 * @param identifier the identifier of the publication
	 * @return A map where the keys are the version and the value the subject of the publication.
	 */
	public Map<String, IRI> determineSubjectsByIdentifier(String identifier, String authToken) {
		Map<String, IRI> result = new HashMap<>();
		
		String query = getVersionsQuery(identifier);

		SimpleProfile profile = getSimpleProfile();
		List<BindingSet> bindingSets = profile.queryOutbox(query.replaceAll("\n", ""), authToken);
		
		bindingSets.forEach(binding->{
			result.put(binding.getValue("version").stringValue(), Values.iri(binding.getValue("objectId").stringValue()));
		});
		return result;		
	}

	/**
	 * Posts the passed activity to <a href="https://linkedopenactors.gitlab.io/rdf-pub/">rdf-pub</a>.
	 * The passed Model is analysed and validated, see {@link PublicationValidator#validatePublication(Model)}, {@link PublicationVersionIncrementChecker#checkOrSet(Publication, Integer)}  
	 * @param activity The activity containing the Publication to create.
	 * @return The IRI (Subject) of the newly created activity.
	 */
	public IRI createPublication(Model activity, String authToken) {
		ModelLogger.debug(log, activity, "createPublication:");
		PublicationValidator.validatePublication(activity);
		Publication toCreatePublication = extractTheOnlyExistingPublication(activity);
		String publicationIdentifier = toCreatePublication.getIdentifier();
		Optional<Integer> latestPublicationVersion = getLatestVersion(publicationIdentifier, authToken);
		PublicationVersionIncrementChecker.checkOrSet(toCreatePublication, latestPublicationVersion.orElse(null));

		Optional<Publication> existingPublicationOptional = latestPublicationVersion
				.flatMap(lv -> getConstructedPublication(publicationIdentifier, lv, authToken));
		
		if(existingPublicationOptional.isPresent()) {
			Publication existingPublication = existingPublicationOptional.get();
			log.trace("publication already existance as Version: " + existingPublication.getVersion()); 
			CreatePublicationMonitor createPublicationMonitor = new CreatePublicationMonitor();			
			toCreatePublication = createPublicationMonitor.monitorCreation(toCreatePublication, existingPublication);
		}
		return rdfPubClient.postActivity(toCreatePublication.getModel(), authToken);
	}

	/**
	 * Reads all <a href="https://en.wikipedia.org/wiki/Semantic_triple">Triples</a> found for the passed <a href="https://en.wikipedia.org/wiki/Semantic_triple#Subject,_predicate_and_object">subject</a>.
	 * @param subject The <a href="https://en.wikipedia.org/wiki/Semantic_triple#Subject,_predicate_and_object">subject</a> of the <a href="https://en.wikipedia.org/wiki/Semantic_triple">Triples</a> to read. 
	 * @return All <a href="https://en.wikipedia.org/wiki/Semantic_triple">Triples</a> found for the passed <a href="https://en.wikipedia.org/wiki/Semantic_triple#Subject,_predicate_and_object">subject</a>.
	 */
	public Optional<ModelAndSubject> read(IRI subject, String authToken) {		
		return rdfPubClient.read(subject, authToken).map(model->new ModelAndSubject(subject, model));
	}

	/**
	 * Gets the result of {@link #getConstructedPublicationHistory(String, String)} for the Publication that is contained in the activity for the given activitySubject.
	 * @param activitySubject The <a href="https://en.wikipedia.org/wiki/Semantic_triple#Subject,_predicate_and_object">subject</a> of the activity that contain a Publication.
	 * @return The result of {@link #getConstructedPublicationHistory(String, String)} for the Publication that is contained in the activity for the given activitySubject.
	 */
	public Optional<PublicationHistory> getPublicationOfActivity(IRI activitySubject, String authToken) {
		ModelAndSubject publication = getObjectOfActivity(activitySubject, authToken).orElseThrow();
		return getIdentifier(publication).flatMap(id->getConstructedPublicationHistory(id, authToken));
	}

	/**
	 * @param activity The model containing the triples of the Publication.
	 * @return The extracted {@link Publication} 
	 * @throws NoSuchElementException if there is no Publication in the given {@link Model}.
	 */
	private Publication extractTheOnlyExistingPublication(Model activity) {
		return PublicationModelsFactory.getPublicationHistoryInstance(activity)
				.map(PublicationHistory::getTheOnlyExistingPublication)
				.orElseThrow(() -> new NoSuchElementException("No Publication present in the passed model"));
	}

	private String getVersionsQuery(String identifier) {
		return "PREFIX schema: <https://schema.org/> \n"
				+ "			PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"
				+ "			PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
				+ "			SELECT DISTINCT *\n"
				+ "			WHERE { \n"
				+ "				?objectId rdf:type ?type .  	\n"
				+ "			  	?objectId schema:identifier ?identifier .\n"
				+ "			    ?objectId schema:version ?version .    \n"
				+ "			    FILTER( ?identifier = \""+identifier+"\" ) .\n"
				+ "			    FILTER( ?type = schema:CreativeWork ) .\n"
				+ "			}";
	}

	
	private SimpleProfile getSimpleProfile() {
		if(profile==null) {
			rdfPubClient.getProfile().orElseThrow(()->new RuntimeException("no profile found."));
			profile = new SimpleProfile(rdfPubClient);
		} 
		return profile;
	}
	
	private Optional<ModelAndSubject> getObjectOfActivity(IRI activitySubject, String authToken) {
		return read(activitySubject, authToken)
				.map(this::getObjectIri)
				.flatMap(iri->read(iri, authToken));
	}

	private Optional<String> getIdentifier(ModelAndSubject publication) {
		return Models.getPropertyLiteral(publication.getModel(), publication.getSubject(), SCHEMA_ORG.identifier)
				.map(Literal::stringValue);
	}
	
	private IRI getObjectIri(ModelAndSubject activityMaS) {
		return Models.getPropertyIRI(activityMaS.getModel(), activityMaS.getSubject(), AS.object).orElseThrow();
	}
	
	public int countAllPlacesInclRevisions(String authToken) {
		String query = """
				PREFIX schema: <https://schema.org/> 
				PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
				PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
				SELECT (count(?identifier) as ?publications)
				WHERE { 
					?subject schema:identifier ?identifier .  	
					?subject schema:version ?version .
				}				
				""";
		
		SimpleProfile profile = getSimpleProfile();
		List<BindingSet> bindingSets = profile.queryOutbox(query, authToken);
		Value publicationsValue = bindingSets.get(0).getBinding("publications").getValue();
		return ((Literal)publicationsValue).intValue();
	}
	
	public int countPublications(String authToken) {
		String query = """
				PREFIX schema: <https://schema.org/> 
				PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
				PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
				SELECT (count(distinct ?identifier) as ?publications)
				WHERE { 
					?subject schema:identifier ?identifier .  	
				}
				""";
		SimpleProfile profile = getSimpleProfile();
		List<BindingSet> bindingSets = profile.queryOutbox(query, authToken);
		Value publicationsValue = bindingSets.get(0).getBinding("publications").getValue();
		return ((Literal)publicationsValue).intValue();
	}
}
