package org.linkedopenactors.loardfpubadapter;

import org.linkedopenactors.loardfpubadapter.model.Publication;

/**
 * Thrown, if somebody tries to add a version of an {@link Publication}, that is already contained.
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
public class AlreadyExistingVersionException extends IllegalStateException {

	private static final long serialVersionUID = 1417749204845272900L;
	private long existingPublicationVersion;
	private Integer versionToAdd;
	
	public AlreadyExistingVersionException() {
		super();
	}

	public AlreadyExistingVersionException(String message, Throwable cause) {
		super(message, cause);
	}

	public AlreadyExistingVersionException(String s) {
		super(s);
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getExistingPublicationVersion() {
		return existingPublicationVersion;
	}

	public void setExistingPublicationVersion(long existingPublicationVersion) {
		this.existingPublicationVersion = existingPublicationVersion;
	}

	public Integer getVersionToAdd() {
		return versionToAdd;
	}

	public void setVersionToAdd(Integer versionToAdd) {
		this.versionToAdd = versionToAdd;
	}
}
