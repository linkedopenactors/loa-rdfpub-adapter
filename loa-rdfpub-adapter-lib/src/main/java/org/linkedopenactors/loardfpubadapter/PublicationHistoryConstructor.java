package org.linkedopenactors.loardfpubadapter;

import static org.eclipse.rdf4j.model.util.Values.literal;

import java.util.Optional;
import java.util.function.Function;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.linkedopenactors.loardfpubadapter.model.Publication;
import org.linkedopenactors.loardfpubadapter.model.PublicationHistory;
import org.linkedopenactors.loardfpubadapter.model.PublicationModelsFactory;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.utils.ModelLogger;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
public class PublicationHistoryConstructor {

	public Optional<PublicationHistory> getConstructedPublication(Function<String, Model> queryable, IRI userId, String identifier) {
		Model m2 = queryable.apply(getConstructQueryPublicationHistory(userId.stringValue(), identifier));
		return PublicationModelsFactory.getPublicationHistoryInstance(m2);
	}
	
	public Optional<Publication> getConstructedPublication(Function<String, Model> queryable, IRI userId, String identifier, long version) {
		String query = getConstructQueryPublication(userId.stringValue(), identifier, version);
		Model publication = queryable.apply(query);
		return getSubject(publication, identifier)
				.map(subject -> new ModelAndSubject(subject, publication))
				.map(mas -> PublicationModelsFactory.getPublicationInstance(mas));
	}

	public Mono<PublicationHistory> getConstructedPublicationWebFlux(Function<String, Mono<Model>> queryable, IRI userId, String identifier) {
		return queryable.apply(getConstructQueryPublicationHistory(userId.stringValue(), identifier))
				.map(x->PublicationModelsFactory.getPublicationHistoryInstance(x).orElse(null));
	}

	public Mono<Publication> getConstructedPublicationWebFlux(Function<String, Mono<Model>> queryable, IRI userId, String identifier, long version) {
		String query = getConstructQueryPublication(userId.stringValue(), identifier, version);
		return queryable.apply(query).map(p->{
			return getSubject(p, identifier)
					.map(subject -> new ModelAndSubject(subject, p))
					.map(mas -> PublicationModelsFactory.getPublicationInstance(mas))
					.orElse(null);
			}
		);
	}

	private Optional<IRI> getSubject(Model publication, String identifier) {
		Model filtered = publication.filter(null, SCHEMA_ORG.identifier, literal(identifier));
		if(filtered.size()>1) {
			ModelLogger.error(log, publication, "model has more than one identifier");
			throw new IllegalStateException("model has more than one identifier.");
		} else if(filtered.isEmpty()) {
			return Optional.empty();
		} else {
			return Optional.of((IRI)filtered.iterator().next().getSubject());
		}
	}

	private String getConstructQueryPublication(String userId, String identifier, long version) {
		return """
				PREFIX schema: <https://schema.org/>
				PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
				PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
				PREFIX as: <https://www.w3.org/ns/activitystreams#>
				
				CONSTRUCT {
					?publication rdf:type ?type ;
				        schema:about ?about ;
				        schema:about ?about;
				        schema:name ?name ;
				        as:name ?asname ;
				        as:published ?published ;
				        as:attributedTo ?attributedTo ;
				        schema:creativeWorkStatus ?creativeWorkStatus ;
				        schema:dateCreated ?dateCreated ;
				        schema:dateModified ?dateModified ;
				        schema:description ?description ;
				        schema:identifier ?identifier ;
				        schema:keywords ?keywords ;
				        schema:license ?license ;
				        schema:version %d .
				        ?about rdf:type ?aboutType ;
				        schema:name ?aboutName ;
				        schema:contactPoint ?aboutContactPoint ;
				        schema:location ?aboutLocation .
				    ?aboutContactPoint rdf:type ?contactPointType ;
				        schema:email ?contactPointEmail ;
				        schema:name ?contactPointName ;
				        schema:telephone?contactPointTelephone .
				    ?aboutLocation rdf:type ?locationType ;
				        schema:latitude ?locationLatitude ;
				      schema:longitude ?locationLongitude ;
				      schema:address ?locationAddress .
				    # Adress
				    ?locationAddress rdf:type ?addressType ;
				    	schema:addressCountry ?addressCountry ;
						schema:addressLocality ?addressLocality ;
				        schema:streetAddress ?addressStreetAddress ;
				        schema:postalCode ?addressPostalCode .
				}
				FROM <%s>
				WHERE {
					?publication rdf:type schema:CreativeWork ;
					rdf:type ?type ;
					schema:version "%d"^^<http://www.w3.org/2001/XMLSchema#int> ;
				    schema:identifier "%s".
				    OPTIONAL {?publication schema:name ?name . }
				    OPTIONAL {?publication as:name ?asname . }
				    OPTIONAL {?publication as:published ?published . }
				    OPTIONAL {?publication as:attributedTo ?attributedTo . }
				    OPTIONAL {?publication schema:creativeWorkStatus ?creativeWorkStatus . }
				    OPTIONAL {?publication schema:dateCreated ?dateCreated . }
				    OPTIONAL {?publication schema:dateModified ?dateModified . }
				    OPTIONAL {?publication schema:description ?description . }
				    OPTIONAL {?publication schema:identifier ?identifier . }
				    OPTIONAL {?publication schema:keywords ?keywords . }
				    OPTIONAL {?publication schema:license ?license . }				     
				    OPTIONAL {?publication schema:about ?about . }
				    # Organization
				    OPTIONAL {
				    	?publication schema:about ?about . 
					    OPTIONAL {?publication schema:about/rdf:type ?aboutType . }
					    OPTIONAL {?publication schema:about/schema:name ?aboutName . }
					    OPTIONAL {?publication schema:about/schema:contactPoint ?aboutContactPoint . }
					    OPTIONAL {?publication schema:about/schema:location ?aboutLocation . }
				    }
				    # ContactPoint
				    OPTIONAL {?publication schema:about/schema:contactPoint/rdf:type ?contactPointType . }
				    OPTIONAL {?publication schema:about/schema:contactPoint/schema:email ?contactPointEmail . }
				    OPTIONAL {?publication schema:about/schema:contactPoint/schema:name ?contactPointName . }
				    OPTIONAL {?publication schema:about/schema:contactPoint/schema:telephone?contactPointTelephone . }
				    # Place
				    OPTIONAL {?publication schema:about/schema:location/rdf:type ?locationType . }
					OPTIONAL {?publication schema:about/schema:location/schema:latitude ?locationLatitude . }
					OPTIONAL {?publication schema:about/schema:location/schema:longitude ?locationLongitude . }
					OPTIONAL {?publication schema:about/schema:location/schema:address ?locationAddress . }
					# Adress
				    OPTIONAL {?publication schema:about/schema:location/schema:address/rdf:type ?addressType . }
				    OPTIONAL {?publication schema:about/schema:location/schema:address/schema:addressLocality ?addressLocality . }
				    OPTIONAL {?publication schema:about/schema:location/schema:address/schema:streetAddress ?addressStreetAddress . }
				    OPTIONAL {?publication schema:about/schema:location/schema:address/schema:postalCode ?addressPostalCode . }
				    OPTIONAL {?publication schema:about/schema:location/schema:address/schema:addressCountry ?addressCountry . }
				} """.formatted(version, userId, version, identifier);
	}

	private String getConstructQueryPublicationHistory(String userId, String identifier) {
		return """
				PREFIX schema: <https://schema.org/>
				PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
				PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
				PREFIX as: <https://www.w3.org/ns/activitystreams#>
				
				CONSTRUCT {
					?publication rdf:type ?type ;
				        schema:about ?about ;
				        schema:about ?about;
				        schema:name ?name ;
				        as:name ?asname ;
				        as:published ?published ;
				        as:attributedTo ?attributedTo ;
				        schema:creativeWorkStatus ?creativeWorkStatus ;
				        schema:dateCreated ?dateCreated ;
				        schema:dateModified ?dateModified ;
				        schema:description ?description ;
				        schema:identifier ?identifier ;
				        schema:keywords ?keywords ;
				        schema:license ?license ;
				        schema:version ?version .
				        ?about rdf:type ?aboutType ;
				        schema:name ?aboutName ;
				        schema:contactPoint ?aboutContactPoint ;
				        schema:location ?aboutLocation .
				    ?aboutContactPoint rdf:type ?contactPointType ;
				        schema:email ?contactPointEmail ;
				        schema:name ?contactPointName ;
				        schema:telephone?contactPointTelephone .
				    ?aboutLocation rdf:type ?locationType ;
				        schema:latitude ?locationLatitude ;
				      schema:longitude ?locationLongitude ;
				      schema:address ?locationAddress .
				    # Adress
				    ?locationAddress rdf:type ?addressType ;
				    	schema:addressCountry ?addressCountry ;
						schema:addressLocality ?addressLocality ;
				        schema:streetAddress ?addressStreetAddress ;
				        schema:postalCode ?addressPostalCode .
				}
				FROM <%s>
				WHERE {
					?publication rdf:type schema:CreativeWork ;
					rdf:type ?type ;
				    schema:identifier "%s".
				    OPTIONAL {?publication schema:name ?name . }
				    OPTIONAL {?publication as:name ?asname . }
				    OPTIONAL {?publication as:published ?published . }
				    OPTIONAL {?publication as:attributedTo ?attributedTo . }
				    OPTIONAL {?publication schema:creativeWorkStatus ?creativeWorkStatus . }
				    OPTIONAL {?publication schema:dateCreated ?dateCreated . }
				    OPTIONAL {?publication schema:dateModified ?dateModified . }
				    OPTIONAL {?publication schema:description ?description . }
				    OPTIONAL {?publication schema:identifier ?identifier . }
				    OPTIONAL {?publication schema:keywords ?keywords . }
				    OPTIONAL {?publication schema:license ?license . }
					OPTIONAL {?publication schema:version ?version . }
				    OPTIONAL {?publication schema:about ?about . }
				    # Organization
				    OPTIONAL {
				    	?publication schema:about ?about . 
					    OPTIONAL {?publication schema:about/rdf:type ?aboutType . }
					    OPTIONAL {?publication schema:about/schema:name ?aboutName . }
					    OPTIONAL {?publication schema:about/schema:contactPoint ?aboutContactPoint . }
					    OPTIONAL {?publication schema:about/schema:location ?aboutLocation . }
				    }
				    # ContactPoint
				    OPTIONAL {?publication schema:about/schema:contactPoint/rdf:type ?contactPointType . }
				    OPTIONAL {?publication schema:about/schema:contactPoint/schema:email ?contactPointEmail . }
				    OPTIONAL {?publication schema:about/schema:contactPoint/schema:name ?contactPointName . }
				    OPTIONAL {?publication schema:about/schema:contactPoint/schema:telephone?contactPointTelephone . }
				    # Place
				    OPTIONAL {?publication schema:about/schema:location/rdf:type ?locationType . }
					OPTIONAL {?publication schema:about/schema:location/schema:latitude ?locationLatitude . }
					OPTIONAL {?publication schema:about/schema:location/schema:longitude ?locationLongitude . }
					OPTIONAL {?publication schema:about/schema:location/schema:address ?locationAddress . }
					# Adress
				    OPTIONAL {?publication schema:about/schema:location/schema:address/rdf:type ?addressType . }
				    OPTIONAL {?publication schema:about/schema:location/schema:address/schema:addressLocality ?addressLocality . }
				    OPTIONAL {?publication schema:about/schema:location/schema:address/schema:streetAddress ?addressStreetAddress . }
				    OPTIONAL {?publication schema:about/schema:location/schema:address/schema:postalCode ?addressPostalCode . }
				    OPTIONAL {?publication schema:about/schema:location/schema:address/schema:addressCountry ?addressCountry . }
				} """.formatted(userId, identifier);
	}
}
