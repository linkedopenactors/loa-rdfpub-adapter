package org.linkedopenactors.loardfpubadapter.model;

import java.util.Optional;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.utils.ModelLogger;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PublicationSimple extends ModelContainer implements Publication {

	private OrganisationSimple organisationSimple;

	public PublicationSimple(ModelAndSubject modelAndSubject) {
		super(modelAndSubject);
		organisationSimple = new OrganisationSimple(new ModelAndSubject(getIri(SCHEMA_ORG.about).orElseThrow(()->new IllegalStateException(SCHEMA_ORG.about + " is mandatory!")), modelAndSubject.getModel()));
	}

	@Override
	public Integer getVersion() {
		return getIntLiteral(SCHEMA_ORG.version).orElseThrow();
	}

	@Override
	public Organisation getOrgansation() {
		return organisationSimple;
	}

	@Override
	public String getCopyrightNotice() {
		return getStringLiteral(SCHEMA_ORG.copyrightNotice).orElseThrow();
	}

	@Override
	public String getCreativeWorkStatus() {
		return getStringLiteral(SCHEMA_ORG.creativeWorkStatus).orElseThrow();
	}

	@Override
	public String getDateCreated() {
		return getStringLiteral(SCHEMA_ORG.dateCreated).orElseThrow();
	}

	@Override
	public Optional<String> getDateModified() {
		return getStringLiteral(SCHEMA_ORG.dateModified);
	}

	@Override
	public String getLicense() {
		return getStringLiteral(SCHEMA_ORG.license).orElseThrow();
	}

	@Override
	public Optional<String> getKeywords() {
		return getStringLiteral(SCHEMA_ORG.keywords);
	}

	@Override
	public String getIdentifier() {
		return getStringLiteral(SCHEMA_ORG.identifier).orElseThrow();
	}

	@Override
	public Optional<String> getDescription() {
		return getStringLiteral(SCHEMA_ORG.description);
	}

	@Override
	public IRI getId() {
		return modelAndSubject.getSubject();
	}

	@Override
	public Model getModel() {
		Model model = new ModelBuilder().build();
		model.addAll(super.getModel());
		model.addAll(getOrgansation().getModel());
		return model;
	}

	@Override
	public void debugDump(String msg) {
		ModelLogger.debug(log, getModel(), msg);
	}

	@Override
	public void traceDump(String msg) {
		ModelLogger.trace(log, getModel(), msg);
	}
}
