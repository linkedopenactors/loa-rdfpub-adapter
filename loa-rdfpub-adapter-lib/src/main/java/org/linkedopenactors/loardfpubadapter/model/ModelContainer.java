package org.linkedopenactors.loardfpubadapter.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;

abstract class ModelContainer {
	protected ModelAndSubject modelAndSubject;
	
	public ModelContainer(ModelAndSubject modelAndSubject) {
		this.modelAndSubject = modelAndSubject;
	}
	
	protected Optional<Literal> getLiteral(IRI predicate) {
		return Models.getPropertyLiteral(modelAndSubject.getModel(), modelAndSubject.getSubject(), predicate);
	}
	
	protected Optional<String> getStringLiteral(IRI predicate) {
		return Models.getPropertyLiteral(modelAndSubject.getModel(), modelAndSubject.getSubject(), predicate).map(Literal::stringValue);
	}
	
	protected Optional<Integer> getIntLiteral(IRI predicate) {
		return Models.getPropertyLiteral(modelAndSubject.getModel(), modelAndSubject.getSubject(), predicate).map(Literal::intValue);
	}

	protected Optional<Double> getDoubleLiteral(IRI predicate) {
		return Models.getPropertyLiteral(modelAndSubject.getModel(), modelAndSubject.getSubject(), predicate).map(Literal::doubleValue);
	}
	
	protected Optional<IRI> getIri(IRI predicate) {
		return Models.getPropertyIRI(modelAndSubject.getModel(), modelAndSubject.getSubject(), predicate);
	}

	protected Set<IRI> getIris(IRI predicate) {
		return Models.getPropertyIRIs(modelAndSubject.getModel(), modelAndSubject.getSubject(), predicate);
	}

	public List<String> getPropertyMap(IRI...predicatesToIgnore) {
		Set<IRI> filter = new HashSet<IRI>(Arrays.asList(predicatesToIgnore));
		Model model = this.modelAndSubject.getModel().filter(this.modelAndSubject.getSubject(), null, null);
		// Do not use #getModel() here, because it is sometimes overwritten in an unusable way here !!
		return model.stream()
				.filter(stmt->!filter.contains(stmt.getPredicate()))
				.map(stmt->stmt.getPredicate() + "::" + stmt.getObject())
				.collect(Collectors.toList());
	}

	public Model getModel() {
		return this.modelAndSubject.getModel().filter(this.modelAndSubject.getSubject(), null, null);
	}
}
