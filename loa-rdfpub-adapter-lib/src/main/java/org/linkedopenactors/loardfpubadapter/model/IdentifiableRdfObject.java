package org.linkedopenactors.loardfpubadapter.model;

import java.util.List;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;

public interface IdentifiableRdfObject {
	IRI getId();	
	List<String> getPropertyMap(IRI...predicatesToIgnore);
	Model getModel();
}
