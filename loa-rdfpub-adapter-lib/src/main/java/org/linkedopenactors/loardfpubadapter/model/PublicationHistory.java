package org.linkedopenactors.loardfpubadapter.model;

import java.util.Map;
import java.util.Optional;

import org.eclipse.rdf4j.model.Model;

/**
 * A container for different versions of a <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a>. 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
public interface PublicationHistory {
	/**
	 * Gets a {@link Map} of versions as key and {@link Publication}s as value.
	 * @return {@link Map} of versions as key and {@link Publication}s as value.
	 */
	Map<Long, Publication> getPublicationsByVersion();
	
	/**
	 * Gets the {@link Publication} with the next larger version.
	 * @param version The version of the current {@link Publication}.
	 * @return The {@link Publication} with the next larger version. 
	 */
	Optional<Publication> nextPublication(int version);

	/**
	 * Gets the {@link Publication} with the next smaller version.
	 * @param version The version of the current {@link Publication}.
	 * @return The {@link Publication} with the next smaller version. 
	 */
	Optional<Publication> previousPublication(int version);
	
	/**
	 * Gets the identifier the contained {@link Publication}.
	 * @return The identifier the contained {@link Publication}.
	 */
	Optional<String> getIdentifier();
	
	/**
	 * Gets the {@link Publication} with the biggest / latest version.
	 * @return The {@link Publication} with the biggest / latest version.
	 */
	Optional<Publication> getLatest();
	
	/**
	 * Gets the number of versions included.
	 * @return The number of versions included.
	 */
	int versionCount();
	
	/**
	 * Checks, that there is exact one version of a {@link Publication} and returns this {@link Publication}.
	 * @return The only existing {@link Publication} in that {@link PublicationHistory}.
	 */
	Publication getTheOnlyExistingPublication();

	/**
	 * Checks, if there is no {@link Publication} in this {@link PublicationHistory}. 
	 * @return True, if there is minimum one version of a {@link Publication}.
	 */
	boolean isEmpty();
	
	/**
	 * Prints the {@link Model} (as Turtle) of all contained Publications to standard out. Log Level is DEBUG. 
	 * @param string A message, that is printed before the model.
	 */
	void dump(String string);
	
	/**
	 * Gets the {@link Model} of all contained {@link Publication}s.
	 * @return The {@link Model} of all contained {@link Publication}s.
	 */
	Model asModel();
}
