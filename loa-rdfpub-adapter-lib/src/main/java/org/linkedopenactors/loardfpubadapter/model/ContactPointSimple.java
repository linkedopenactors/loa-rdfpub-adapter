package org.linkedopenactors.loardfpubadapter.model;

import java.util.Optional;

import org.eclipse.rdf4j.model.IRI;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

/**
 * Implementation of {@link ContactPoint}
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
public class ContactPointSimple extends ModelContainer implements ContactPoint {
	
	public ContactPointSimple(ModelAndSubject modelAndSubject) {
		super(modelAndSubject);
	}

	@Override
	public Optional<String> getEmail() {
		return getStringLiteral(SCHEMA_ORG.email);
	}

	@Override
	public Optional<String> getName() {
		return getStringLiteral(SCHEMA_ORG.name);
	}

	@Override
	public Optional<String> getTelephone() {
		return getStringLiteral(SCHEMA_ORG.telephone);
	}

	@Override
	public IRI getId() {
		return modelAndSubject.getSubject();
	}
}
