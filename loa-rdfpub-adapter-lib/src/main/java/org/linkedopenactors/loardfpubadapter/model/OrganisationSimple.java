package org.linkedopenactors.loardfpubadapter.model;

import java.util.Optional;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

public class OrganisationSimple extends ModelContainer implements Organisation {

	private IRI locationSubject;
	private IRI contactPointSubject;

	public OrganisationSimple(ModelAndSubject modelAndSubject) {
		super(modelAndSubject);
		locationSubject = getIri(SCHEMA_ORG.location).orElse(null);
		contactPointSubject = getIri(SCHEMA_ORG.contactPoint).orElse(null);
	}

	@Override
	public Optional<String> getLegalName() {
		return getStringLiteral(SCHEMA_ORG.legalName);
	}

	@Override
	public Optional<Place> getLocation() {
		if(locationSubject!=null) {
			return Optional.of(new PlaceSimple(new ModelAndSubject(locationSubject, modelAndSubject.getModel())));	
		} else {
			return Optional.empty();
		}
	}

	@Override
	public Optional<String> getName() {
		return getStringLiteral(SCHEMA_ORG.name);
	}

	@Override
	public Set<IRI> getUrls() {
		return getIris(SCHEMA_ORG.url);
	}

	@Override
	public Optional<ContactPoint> getContactPoint() {
		if(contactPointSubject!=null) {
			return Optional.of(new ContactPointSimple(new ModelAndSubject(contactPointSubject, modelAndSubject.getModel())));	
		} else {
			return Optional.empty();
		}
	}

	@Override
	public IRI getId() {
		return modelAndSubject.getSubject();
	}
		
	@Override
	public Model getModel() {
		Model model = new ModelBuilder().build();
		model.addAll(super.getModel());
		getLocation().ifPresent(place->model.addAll(place.getModel()));
		getContactPoint().ifPresent(contactPoint->model.addAll(contactPoint.getModel()));
		return model;
	}

	@Override
	public Optional<IRI> getLocationSubject() {
		return Optional.ofNullable(locationSubject);
	}

	@Override
	public Optional<IRI> getContactPointSubject() {
		return Optional.ofNullable(contactPointSubject);
	}
}
