package org.linkedopenactors.loardfpubadapter.model;

import java.util.Optional;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

public class PlaceSimple extends ModelContainer implements Place {

	private IRI postalAddressSubject;
	
	public PlaceSimple(ModelAndSubject modelAndSubject) {
		super(modelAndSubject);
		postalAddressSubject = getIri(SCHEMA_ORG.address).orElse(null);
	}

	@Override
	public Double getLatitude() {
		return getDoubleLiteral(SCHEMA_ORG.latitude).orElseThrow(()->new IllegalStateException(SCHEMA_ORG.latitude + " is mandatory!"));
	}

	@Override
	public Double getLongitude() {
		return getDoubleLiteral(SCHEMA_ORG.longitude).orElseThrow(()->new IllegalStateException(SCHEMA_ORG.longitude + " is mandatory!"));
	}

	@Override
	public Optional<PostalAddress> getPostalAddress() {
		if(postalAddressSubject!=null) {
			return Optional.of(new PostalAddressSimple(new ModelAndSubject(postalAddressSubject, modelAndSubject.getModel())));
		} else {
			return Optional.empty();
		}
	}

	@Override
	public IRI getId() {
		return modelAndSubject.getSubject();
	}

	@Override
	public Model getModel() {
		Model model = new ModelBuilder().build();
		model.addAll(super.getModel());
		getPostalAddress().ifPresent(postalAddress->model.addAll(postalAddress.getModel()));
		return model;
	}

	@Override
	public Optional<IRI> getPostalAddressSubject() {
		return Optional.ofNullable(postalAddressSubject);
	}
}
