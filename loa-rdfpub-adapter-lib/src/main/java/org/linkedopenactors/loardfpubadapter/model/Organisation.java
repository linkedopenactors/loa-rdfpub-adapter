package org.linkedopenactors.loardfpubadapter.model;

import java.util.Optional;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;

public interface Organisation extends IdentifiableRdfObject {
	Optional<String> getLegalName();	
	Optional<Place> getLocation();
	Optional<IRI> getLocationSubject();
	Optional<String> getName();
	Set<IRI> getUrls();
	Optional<ContactPoint> getContactPoint();
	Optional<IRI> getContactPointSubject();
}
