package org.linkedopenactors.loardfpubadapter.model;

import java.util.Optional;

import org.eclipse.rdf4j.model.Model;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;

public class PublicationModelsFactory {

	public static Optional<PublicationHistory> getPublicationHistoryInstance(Model model) {
		return model.isEmpty() ? Optional.empty() : Optional.of(new PublicationHistorySimple(model)); 
	}

	public static Publication getPublicationInstance(ModelAndSubject modelAndSubject) {
		return new PublicationSimple(modelAndSubject); 
	}
}
