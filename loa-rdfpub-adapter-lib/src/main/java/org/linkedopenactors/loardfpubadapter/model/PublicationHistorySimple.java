package org.linkedopenactors.loardfpubadapter.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.linkedopenactors.loardfpubadapter.NextPrevProcessing;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.utils.ModelLogger;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class PublicationHistorySimple implements PublicationHistory {

	Map<Long, Publication> publicationByVersion;
	NextPrevProcessing<Publication> publicationNextPrevProcessing;
	private Model model;
		
	public PublicationHistorySimple(Model modelParam) {
		this.model = clone(modelParam);
		publicationByVersion = new HashMap<>();
		this.model.filter(null, RDF.TYPE, SCHEMA_ORG.CreativeWork).stream()
			.map(Statement::getSubject)
			.map(subject->(IRI)subject)			
			.distinct()
			.forEach(subject->{
				Long version = Models.getPropertyLiteral(this.model, subject, SCHEMA_ORG.version)
						.orElseThrow(()->new IllegalStateException("Property " + SCHEMA_ORG.version + " missing!"))
						.longValue();
				PublicationSimple publication = new PublicationSimple(new ModelAndSubject(subject, this.model));
				publicationByVersion.put(version, publication);
				});		
		publicationNextPrevProcessing = new NextPrevProcessing<>(publicationByVersion);
	}

	private Model clone(Model modelParam) {
		Model model = new ModelBuilder().build();
		model.addAll(modelParam);
		return model;
	}

	@Override
	public Map<Long, Publication> getPublicationsByVersion() {
		return publicationByVersion;
	}

	@Override
	public Optional<Publication> nextPublication(int version) {
		return publicationNextPrevProcessing.getNext(version);
	}

	@Override
	public Optional<Publication> previousPublication(int version) {
		return publicationNextPrevProcessing.getPrev(version);
	}

	@Override
	public Optional<String> getIdentifier() {
		return getLatest().map(pub->pub.getIdentifier());
	}

	@Override
	public Optional<Publication> getLatest() {
		return publicationNextPrevProcessing.getMax();
	}

	@Override
	public Publication getTheOnlyExistingPublication() {
		int size = getPublicationByVersion().size();
		if(size!=1) {
			throw new IllegalStateException("make sure, that there is really only one publication, befor you are using this method!");
		}
		return getLatest().orElseThrow();
	}

	@Override
	public int versionCount() {
		int size = getPublicationByVersion().size();
		return size;
	}

	@Override
	public boolean isEmpty() {
		return this.publicationByVersion.isEmpty();
	}

	@Override
	public void dump(String msg) {
		ModelLogger.debug(log, model, msg);
	}

	@Override
	public Model asModel() {
		return model;		
	}
}
