package org.linkedopenactors.loardfpubadapter.model;

import java.util.Optional;

/**
 * A <a href="https://schema.org/ContactPoint">ContactPoint</a>. 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
public interface ContactPoint extends IdentifiableRdfObject {
	/**
	 * @return <a href="https://schema.org/email">Email</a>
	 */
	Optional<String> getEmail();

	/**
	 * @return <a href="https://schema.org/name">Name</a>
	 */
	Optional<String> getName();
	
	/**
	 * @return <a href="https://schema.org/telephone">Telephone</a>
	 */
	Optional<String> getTelephone()	;
}
