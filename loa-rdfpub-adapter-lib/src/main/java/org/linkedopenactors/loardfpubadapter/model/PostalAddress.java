package org.linkedopenactors.loardfpubadapter.model;

import java.util.Optional;

public interface PostalAddress extends IdentifiableRdfObject {
	/**
	 * e.g. 82333 
	 */
	Optional<String> getPostalCode();
	
	/**
	 * e.g. Munich
	 */
	Optional<String> getAddressLocality();
	
	/**
	 * e.g. Bavaria 
	 */
	Optional<String> getAddressRegion();
	
	/**
	 * e.g. Germany
	 */
	Optional<String> getAddressCountry();
	
	/**
	 * e.g. Wasserburger Landstrasse 263
	 */
	Optional<String> getStreetAddress();
}
