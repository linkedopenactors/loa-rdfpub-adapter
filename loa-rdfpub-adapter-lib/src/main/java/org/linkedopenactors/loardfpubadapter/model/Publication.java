package org.linkedopenactors.loardfpubadapter.model;

import java.util.Optional;

import org.eclipse.rdf4j.model.Model;

/**
 * A <a href="https://schema.org/CreativeWork">CreativeWork</a> that represents a <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a>. 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
public interface Publication extends IdentifiableRdfObject {
	
	/**
	 * Gets the <a href="https://schema.org/version">version</a> of the {@link Publication}.
	 * @return The version.
	 */
	Integer getVersion();

	/**
	 * Gets the reference to the <a href="https://schema.org/Organization">Organsation</a>.
	 * @return The {@link Organisation}
	 */
	Organisation getOrgansation();

	/**
	 * Gets the <a href="https://schema.org/copyrightNotice">CopyrightNotice</a> of this {@link Publication}.
	 * @return The CopyrightNotice.
	 */
	String getCopyrightNotice();

	/**
	 * Gets the <a href="https://schema.org/creativeWorkStatus">CreativeWorkStatus</a> of this {@link Publication}.
	 * @return The CreativeWorkStatus.
	 */
	String getCreativeWorkStatus();

	/**
	 * Gets the <a href="https://schema.org/dateCreated">DateCreated</a> of this {@link Publication}.
	 * @return The DateCreated.
	 */
	String getDateCreated();

	/**
	 * Gets the <a href="https://schema.org/dateModified">DateModified</a> of this {@link Publication}.
	 * @return The DateModified.
	 */
	Optional<String> getDateModified();

	/**
	 * Gets the <a href="https://schema.org/license">License</a> of this {@link Publication}.
	 * @return The License.
	 */
	String getLicense();

	/**
	 * Gets the <a href="https://schema.org/keywords">Keywords</a> of this {@link Publication}.
	 * @return The Keywords.
	 */
	Optional<String> getKeywords();

	/**
	 * Gets the <a href="https://schema.org/identifier">Identifier</a> of this {@link Publication}.
	 * @return The Identifier.
	 */
	String getIdentifier();

	/**
	 * Gets the <a href="https://schema.org/description">Description</a> of this {@link Publication}.
	 * @return The Description.
	 */
	Optional<String> getDescription();

	/**
	 * Prints the {@link Model} (as Turtle) of the Publications to standard out. Log Level is TRACE. 
	 * @param msg A message, that is printed before the model.
	 */
	void traceDump(String msg);
	
	/**
	 * Prints the {@link Model} (as Turtle) of the Publications to standard out. Log Level is DEBUG. 
	 * @param msg A message, that is printed before the model.
	 */
	void debugDump(String msg);
}
