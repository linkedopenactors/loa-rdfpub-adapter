package org.linkedopenactors.loardfpubadapter.model;

import java.util.Optional;

import org.eclipse.rdf4j.model.IRI;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

public class PostalAddressSimple extends ModelContainer implements PostalAddress {

	public PostalAddressSimple(ModelAndSubject modelAndSubject) {
		super(modelAndSubject);
	}

	@Override
	public Optional<String> getPostalCode() {
		return getStringLiteral(SCHEMA_ORG.postalCode);
	}

	@Override
	public Optional<String> getAddressLocality() {
		return getStringLiteral(SCHEMA_ORG.addressLocality);
	}

	@Override
	public Optional<String> getAddressRegion() {
		return getStringLiteral(SCHEMA_ORG.addressRegion);
	}

	@Override
	public Optional<String> getAddressCountry() {
		return getStringLiteral(SCHEMA_ORG.addressCountry);
	}

	@Override
	public Optional<String> getStreetAddress() {
		return getStringLiteral(SCHEMA_ORG.streetAddress);
	}

	@Override
	public IRI getId() {
		return modelAndSubject.getSubject();
	}
}
