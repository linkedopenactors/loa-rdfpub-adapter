package org.linkedopenactors.loardfpubadapter.model;

import java.util.Optional;

import org.eclipse.rdf4j.model.IRI;

public interface Place extends IdentifiableRdfObject {
	Double getLatitude();
	Double getLongitude();
	Optional<PostalAddress> getPostalAddress();
	Optional<IRI> getPostalAddressSubject();
}
