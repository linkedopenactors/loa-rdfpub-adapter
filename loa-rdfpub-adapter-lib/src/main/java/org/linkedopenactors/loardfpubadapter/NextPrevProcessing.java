package org.linkedopenactors.loardfpubadapter;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class NextPrevProcessing <T> {

	private Map<Long, T> map = new HashMap<>();

	public NextPrevProcessing(Map<Long, T> map) {
		this.map = map;
	}
	
	public Optional<T> getNext(long version) {
		long maxVersion = getMaxVersion().orElse(-1L);
		if(version<0||version==maxVersion||map== null||map.isEmpty())  {
			return Optional.empty();
		} else {
			return getMaxVersion()					
					.filter(maxVal->version<maxVal)
					.map(it->version+1)
					.map(map::get);
		}
	}

	private Optional<Long> getMaxVersion() {
		if(map== null||map.isEmpty()) { 
			return Optional.empty(); 
		} else {
			return map.keySet().stream()
					.max(Comparator.comparing(Long::valueOf));
		}
	}
	
	public Optional<T> getPrev(long version) {
		if(map== null||map.isEmpty()) {
			return Optional.empty();
		} else {
			long max = getMaxVersion().orElseThrow();
			if(version<=0||version>max)  {
				return Optional.empty();
			} else {
				return Optional.ofNullable(map.get(version-1));
			}
		}
	}

	public Optional<T> getMax() {
		return getMaxVersion().map(max->this.map.get(max));
	}
	}
