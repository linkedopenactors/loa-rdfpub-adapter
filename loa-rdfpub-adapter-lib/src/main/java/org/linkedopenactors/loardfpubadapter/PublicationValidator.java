package org.linkedopenactors.loardfpubadapter;

import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;

import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

public class PublicationValidator {

	public static void validatePublication(Model activity) {
		checkContainsExactOneCreativeWork(activity);
		checkContainsIdentifier(activity);
		checkContainsExactOneOrganization(activity);
		checkContainsExactOnePlace(activity);
		checkContainsMaxOnePostalAddress(activity);
		checkContainsMaxOneContactPoint(activity);
	}

	
	private static void checkContainsExactOneCreativeWork(Model activity) {
		Model typesOnly = activity.filter(null, RDF.TYPE, SCHEMA_ORG.CreativeWork);
		if(typesOnly.size() != 1) {
			List<String> currentTypes = activity.filter(null, RDF.TYPE, null).stream()
					.map(stmt->stmt.getObject().stringValue())
					.collect(Collectors.toList());	
			throw new IllegalStateException("We expect exact one subject with type " + SCHEMA_ORG.CreativeWork + ", but is: " + currentTypes);
		}
	}
	
	private static void checkContainsExactOneOrganization(Model activity) {
		Model typesOnly = activity.filter(null, RDF.TYPE, SCHEMA_ORG.Organization);
		if(typesOnly.size() != 1) {
			List<String> currentTypes =  activity.filter(null, RDF.TYPE, null).stream()
					.map(stmt->stmt.getObject().stringValue())
					.collect(Collectors.toList());			
			throw new IllegalStateException("We expect exact one subject with type " + SCHEMA_ORG.Organization + ", but is: " + currentTypes);
		}
	}

	private static void checkContainsExactOnePlace(Model activity) {
		Model typesOnly = activity.filter(null, RDF.TYPE, SCHEMA_ORG.Place);
		if(typesOnly.size() != 1) {
			List<String> currentTypes =  activity.filter(null, RDF.TYPE, null).stream()
					.map(stmt->stmt.getObject().stringValue())
					.collect(Collectors.toList());			
			throw new IllegalStateException("We expect exact one subject with type " + SCHEMA_ORG.Place + ", but is: " + currentTypes);
		}
		IRI placeSubject = (IRI)typesOnly.stream().findFirst().orElseThrow().getSubject();
		Models.getPropertyLiteral(activity, placeSubject, SCHEMA_ORG.latitude).orElseThrow(()->new IllegalStateException(placeSubject + " must have an property " + SCHEMA_ORG.latitude));
		Models.getPropertyLiteral(activity, placeSubject, SCHEMA_ORG.longitude).orElseThrow(()->new IllegalStateException(placeSubject + " must have an property " + SCHEMA_ORG.longitude));
	}

	private static void checkContainsMaxOnePostalAddress(Model activity) {
		Model typesOnly = activity.filter(null, RDF.TYPE, SCHEMA_ORG.PostalAddress);
		if(typesOnly.size() > 1) {
			List<String> currentTypes =  activity.filter(null, RDF.TYPE, null).stream()
					.map(stmt->stmt.getObject().stringValue())
					.collect(Collectors.toList());			
			throw new IllegalStateException("We expect maximal one subject with type " + SCHEMA_ORG.PostalAddress + ", but is: " + currentTypes);
		}
	}
	
	private static void checkContainsMaxOneContactPoint(Model activity) {
		Model typesOnly = activity.filter(null, RDF.TYPE, SCHEMA_ORG.ContactPoint);
		if(typesOnly.size() > 1) {
			List<String> currentTypes =  activity.filter(null, RDF.TYPE, null).stream()
					.map(stmt->stmt.getObject().stringValue())
					.collect(Collectors.toList());			
			throw new IllegalStateException("We expect maximal one subject with type " + SCHEMA_ORG.ContactPoint + ", but is: " + currentTypes);
		}
	}

	private static void checkContainsIdentifier(Model activity) {
		Model identifiers = activity.filter(null, SCHEMA_ORG.identifier, null);
		if(identifiers.size() != 1) {
			throw new IllegalStateException("We expect exact one subject with type " + SCHEMA_ORG.identifier + ", but is: " + identifiers);
		}
	}
}
