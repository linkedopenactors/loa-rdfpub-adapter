package org.linkedopenactors.loardfpubadapter;

import java.util.List;

import org.linkedopenactors.loardfpubadapter.model.Publication;

import de.naturzukunft.rdf4j.vocabulary.AS;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

public class ComparatorOrganisation {
	public static boolean hasIdenticalOrganisation(Publication oldPublication, Publication newPublication) {
		List<String> oldPropertyMap = oldPublication.getOrgansation().getPropertyMap(AS.published, AS.attributedTo, SCHEMA_ORG.contactPoint, SCHEMA_ORG.location);
		List<String> newPropertyMap = newPublication.getOrgansation().getPropertyMap(AS.published, AS.attributedTo, SCHEMA_ORG.contactPoint, SCHEMA_ORG.location);
		return !ComparatorPropertyMap.hasChanges(ComparatorOrganisation.class.getSimpleName(), oldPropertyMap, newPropertyMap);		
	}
}
