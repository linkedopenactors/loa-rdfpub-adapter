package org.linkedopenactors.loardfpubadapter;

import java.util.NoSuchElementException;
import java.util.function.Function;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Value;
import org.linkedopenactors.loardfpubadapter.model.Publication;
import org.linkedopenactors.loardfpubadapter.model.PublicationHistory;
import org.linkedopenactors.loardfpubadapter.model.PublicationModelsFactory;
import org.linkedopenactors.rdfpub.client.RdfPubClientWebFlux;

import de.naturzukunft.rdf4j.utils.ModelLogger;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.math.MathFlux;

/**
 * {@link LoaRdfPubAdapterWebFlux} supports functionality needed for <a href="https://linkedopenactors.org">linkedopenactors</a> apps.
 * It's an abstraction with convenience methods for using <a href="https://linkedopenactors.gitlab.io/rdf-pub/">rdf-pub</a>.
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Slf4j
public class LoaRdfPubAdapterWebFlux {

	private RdfPubClientWebFlux rdfPubClient;
	
	public LoaRdfPubAdapterWebFlux(RdfPubClientWebFlux rdfPubClient) {
		this.rdfPubClient = rdfPubClient;
	}

	/**
	 * Gets a set of <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a> versions for the given identifier.
	 * @param identifier The identifier of the <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a>
	 * @return A set of <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a> versions for the given identifier.
	 */
	public Flux<Integer> getVersions(String identifier, String authToken) {
		String query = """
				PREFIX schema: <https://schema.org/> 
				PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
				PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
				SELECT DISTINCT ?identifier ?version
				WHERE { 
				  		?subject rdf:type schema:CreativeWork .  	
				  		?subject schema:identifier "%s" .
				  		?subject schema:version ?version .  		
				  }	
				""".formatted(identifier);
		
		return rdfPubClient.tupleQueryOutbox(query, authToken)
			.map(bs->bs.getValue("version"))
			.filter(Value::isLiteral)
			.map(v->((Literal)v).intValue());
	}

	/**
	 * Gets the latest/biggest <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a> version for the given identifier.
	 * @param identifier The identifier of the <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a>
	 * @return The latest/biggest <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a> version for the given identifier.
	 */
	public Mono<Integer> getLatestVersion(String identifier, String authToken) {
		return MathFlux.max(getVersions(identifier, authToken));		
	}

	/**
	 * Gets the {@link PublicationHistory} for the given identifier.
	 * @param identifier The identifier of the <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a>
	 * @return The {@link PublicationHistory} for the given identifier.
	 */
	public Mono<PublicationHistory> getConstructedPublicationHistory(String identifier, String authToken) {
		Function<String, Mono<Model>> queryable = query -> rdfPubClient.graphQueryOutbox(query, authToken);
		return new PublicationHistoryConstructor()
				.getConstructedPublicationWebFlux(queryable, rdfPubClient.getActorId(), identifier);
	}

	/**
	 * Gets the {@link Publication} for the given identifier and the given version.
	 * @param identifier The identifier of the <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a>
	 * @param version The version of the wanted <a href="https://linkedopenactors.gitlab.io/loa-specification/#publication-2">Publication</a>
	 * @return The {@link Publication} for the given identifier and the given version. 
	 */
	public Mono<Publication> getConstructedPublication(String identifier, long version, String authToken) {
		Function<String, Mono<Model>> queryable = query -> rdfPubClient.graphQueryOutbox(query, authToken);
		return new PublicationHistoryConstructor()
				.getConstructedPublicationWebFlux(queryable, rdfPubClient.getActorId(), identifier, version);
	}

	/**
	 * Posts the passed activity to <a href="https://linkedopenactors.gitlab.io/rdf-pub/">rdf-pub</a>.
	 * The passed Model is analysed and validated, see {@link PublicationValidator#validatePublication(Model)}, {@link PublicationVersionIncrementChecker#checkOrSet(Publication, Integer)}  
	 * @param activity The activity containing the Publication to create.
	 * @return The IRI (Subject) of the newly created activity.
	 */
	
	public Mono<IRI> createPublication(Model activity, String authToken) {
		System.out.println("################################");
		System.out.println("createPublication");
		System.out.println("################################");
		ModelLogger.debug(log, activity, "createPublication:");
		PublicationValidator.validatePublication(activity);
		Publication toCreatePublication = extractTheOnlyExistingPublication(activity);
		String publicationIdentifier = toCreatePublication.getIdentifier();
		return getLatestVersion(publicationIdentifier, authToken)
				.doOnNext(latest->PublicationVersionIncrementChecker.checkOrSet(toCreatePublication, latest))
				.flatMap(lv -> getConstructedPublication(publicationIdentifier, lv, authToken))
				.map(existingPublication->{
					log.trace("publication already existance as Version: " + existingPublication.getVersion());
					CreatePublicationMonitor createPublicationMonitor = new CreatePublicationMonitor();
					return createPublicationMonitor.monitorCreation(toCreatePublication, existingPublication);
					}
				).or(Mono.just(toCreatePublication))
				.flatMap(toCreate->rdfPubClient.postActivity(toCreate.getModel(), authToken));
	}

	/**
	 * @param activity The model containing the triples of the Publication.
	 * @return The extracted {@link Publication} 
	 * @throws NoSuchElementException if there is no Publication in the given {@link Model}.
	 */
	private Publication extractTheOnlyExistingPublication(Model activity) {
		return PublicationModelsFactory.getPublicationHistoryInstance(activity)
				.map(PublicationHistory::getTheOnlyExistingPublication)
				.orElseThrow(() -> new NoSuchElementException("No Publication present in the passed model"));
	}
}
