package org.linkedopenactors.loardfpubadapter;

import static org.eclipse.rdf4j.model.util.Values.literal;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;

import de.naturzukunft.rdf4j.vocabulary.AS;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

class TestPublicationValidator {

	@Test
	void testWithoutType() {
		Model model = new ModelBuilder()
				.subject("http://example.org")
				.build();
		Exception exception = assertThrows(IllegalStateException.class, () -> {
			PublicationValidator.validatePublication(model);	
	    });		
	    assertEquals("We expect exact one subject with type https://schema.org/CreativeWork, but is: []", exception.getMessage());
	}

	@Test
	void testWithoutCreativeWork() {
		Model model = new ModelBuilder()
				.subject("http://example.org")
				.add(RDF.TYPE, AS.Object)
				.build();
		Exception exception = assertThrows(IllegalStateException.class, () -> {
			PublicationValidator.validatePublication(model);	
	    });		
	    assertEquals("We expect exact one subject with type https://schema.org/CreativeWork, but is: [https://www.w3.org/ns/activitystreams#Object]", exception.getMessage());
	}

	@Test
	void testWithoutIdentifier() {
		Model model = new ModelBuilder()
				.subject("http://example.org")
				.add(RDF.TYPE, SCHEMA_ORG.CreativeWork)
				.build();
		Exception exception = assertThrows(IllegalStateException.class, () -> {
			PublicationValidator.validatePublication(model);	
	    });		
	    assertEquals("We expect exact one subject with type https://schema.org/identifier, but is: []", exception.getMessage());
	}

	@Test
	void testWithTwoCreativeWorks() {
		Model model = new ModelBuilder()
				.subject("http://example.org/1")
					.add(RDF.TYPE, SCHEMA_ORG.CreativeWork)
				.subject("http://example.org/2")
					.add(RDF.TYPE, SCHEMA_ORG.CreativeWork)
				.build();
		Exception exception = assertThrows(IllegalStateException.class, () -> {
			PublicationValidator.validatePublication(model);	
	    });		
	    assertEquals("We expect exact one subject with type https://schema.org/CreativeWork, but is: [https://schema.org/CreativeWork, https://schema.org/CreativeWork]", exception.getMessage());
	}
	
	@Test
	void testWithoutOrganization() {
		Model model = new ModelBuilder()
				.subject("http://example.org")
				.add(RDF.TYPE, SCHEMA_ORG.CreativeWork)
				.add(SCHEMA_ORG.identifier, literal(815))
				.build();
		Exception exception = assertThrows(IllegalStateException.class, () -> {
			PublicationValidator.validatePublication(model);	
	    });		
	    assertEquals("We expect exact one subject with type https://schema.org/Organization, but is: [https://schema.org/CreativeWork]", exception.getMessage());
	}

	@Test
	void testWithTwoOrganizations() {
		Model model = new ModelBuilder()
				.subject("http://example.org/1")
					.add(RDF.TYPE, SCHEMA_ORG.Organization)
					.subject("http://example.org/2")
					.add(RDF.TYPE, SCHEMA_ORG.Organization)
				.build();
		Exception exception = assertThrows(IllegalStateException.class, () -> {
			PublicationValidator.validatePublication(model);	
	    });		
	    assertEquals("We expect exact one subject with type https://schema.org/CreativeWork, but is: [https://schema.org/Organization, https://schema.org/Organization]", exception.getMessage());
	}
	
	@Test
	void testWithoutPlace() {
		Model model = new ModelBuilder()
				.subject("http://example.org")
					.add(RDF.TYPE, SCHEMA_ORG.CreativeWork)
					.add(SCHEMA_ORG.identifier, literal(815))
				.subject("http://example.org/2")
					.add(RDF.TYPE, SCHEMA_ORG.Organization)
				.build();
		Exception exception = assertThrows(IllegalStateException.class, () -> {
			PublicationValidator.validatePublication(model);	
	    });		
	    assertEquals("We expect exact one subject with type https://schema.org/Place, but is: [https://schema.org/CreativeWork, https://schema.org/Organization]", exception.getMessage());
	}

	@Test
	void testWithTwoPlaces() {
		Model model = new ModelBuilder()
				.subject("http://example.org/1")
					.add(RDF.TYPE, SCHEMA_ORG.CreativeWork)
					.add(SCHEMA_ORG.identifier, literal(815))
				.subject("http://example.org/2")
					.add(RDF.TYPE, SCHEMA_ORG.Organization)
				.subject("http://example.org/3")
					.add(RDF.TYPE, SCHEMA_ORG.Place)
				.subject("http://example.org/4")
					.add(RDF.TYPE, SCHEMA_ORG.Place)
				.build();
		Exception exception = assertThrows(IllegalStateException.class, () -> {
			PublicationValidator.validatePublication(model);	
	    });		
	    assertEquals("We expect exact one subject with type https://schema.org/Place, but is: [https://schema.org/CreativeWork, https://schema.org/Organization, https://schema.org/Place, https://schema.org/Place]", exception.getMessage());
	}

	@Test
	void testWithTwoPostalAddress() {
		Model model = new ModelBuilder()
				.subject("http://example.org/1")
					.add(RDF.TYPE, SCHEMA_ORG.CreativeWork)
					.add(SCHEMA_ORG.identifier, literal(815))
				.subject("http://example.org/2")
					.add(RDF.TYPE, SCHEMA_ORG.Organization)
				.subject("http://example.org/3")
					.add(RDF.TYPE, SCHEMA_ORG.Place)
					.add(SCHEMA_ORG.latitude, literal(Double.valueOf("4.813279429363683E1")))
					.add(SCHEMA_ORG.longitude, literal(Double.valueOf("1.1676040828077141E1"))) 
				.subject("http://example.org/4")
					.add(RDF.TYPE, SCHEMA_ORG.PostalAddress)
				.subject("http://example.org/5")
					.add(RDF.TYPE, SCHEMA_ORG.PostalAddress)
				.build();
		Exception exception = assertThrows(IllegalStateException.class, () -> {
			PublicationValidator.validatePublication(model);	
	    });		
	    assertEquals("We expect maximal one subject with type https://schema.org/PostalAddress, but is: [https://schema.org/CreativeWork, https://schema.org/Organization, https://schema.org/Place, https://schema.org/PostalAddress, https://schema.org/PostalAddress]", exception.getMessage());
	}

	@Test
	void testWithTwoContactPoint() {
		Model model = new ModelBuilder()
				.subject("http://example.org/1")
					.add(RDF.TYPE, SCHEMA_ORG.CreativeWork)
					.add(SCHEMA_ORG.identifier, literal(815))
				.subject("http://example.org/2")
					.add(RDF.TYPE, SCHEMA_ORG.Organization)
				.subject("http://example.org/3")
					.add(RDF.TYPE, SCHEMA_ORG.Place)
					.add(SCHEMA_ORG.latitude, literal(Double.valueOf("4.813279429363683E1")))
					.add(SCHEMA_ORG.longitude, literal(Double.valueOf("1.1676040828077141E1"))) 
				.subject("http://example.org/4")
					.add(RDF.TYPE, SCHEMA_ORG.PostalAddress)
				.subject("http://example.org/5")
					.add(RDF.TYPE, SCHEMA_ORG.ContactPoint)
				.subject("http://example.org/6")
					.add(RDF.TYPE, SCHEMA_ORG.ContactPoint)
				.build();
		Exception exception = assertThrows(IllegalStateException.class, () -> {
			PublicationValidator.validatePublication(model);	
	    });		
	    assertEquals("We expect maximal one subject with type https://schema.org/ContactPoint, but is: [https://schema.org/CreativeWork, https://schema.org/Organization, https://schema.org/Place, https://schema.org/PostalAddress, https://schema.org/ContactPoint, https://schema.org/ContactPoint]", exception.getMessage());
	}

	@Test
	void testWithoutLongitude() {
		Model model = new ModelBuilder()
				.subject("http://example.org/1")
					.add(RDF.TYPE, SCHEMA_ORG.CreativeWork)
					.add(SCHEMA_ORG.identifier, literal(815))
				.subject("http://example.org/2")
					.add(RDF.TYPE, SCHEMA_ORG.Organization)
				.subject("http://example.org/3")
					.add(RDF.TYPE, SCHEMA_ORG.Place)
					.add(SCHEMA_ORG.latitude, literal(Double.valueOf("4.813279429363683E1")))
				.build();
		Exception exception = assertThrows(IllegalStateException.class, () -> {
			PublicationValidator.validatePublication(model);	
	    });		
	    assertEquals("http://example.org/3 must have an property https://schema.org/longitude", exception.getMessage());
	}

	@Test
	void testWithoutLatitude() {
		Model model = new ModelBuilder()
				.subject("http://example.org/1")
					.add(RDF.TYPE, SCHEMA_ORG.CreativeWork)
					.add(SCHEMA_ORG.identifier, literal(815))
				.subject("http://example.org/2")
					.add(RDF.TYPE, SCHEMA_ORG.Organization)
				.subject("http://example.org/3")
					.add(RDF.TYPE, SCHEMA_ORG.Place)					
					.add(SCHEMA_ORG.longitude, literal(Double.valueOf("1.1676040828077141E1")))
				.build();
		Exception exception = assertThrows(IllegalStateException.class, () -> {
			PublicationValidator.validatePublication(model);	
	    });		
	    assertEquals("http://example.org/3 must have an property https://schema.org/latitude", exception.getMessage());
	}

	@Test
	void testSuccess() {
		Model model = new ModelBuilder()
				.subject("http://example.org/1")
					.add(RDF.TYPE, SCHEMA_ORG.CreativeWork)
					.add(SCHEMA_ORG.identifier, literal(815))
					.subject("http://example.org/2")
					.add(RDF.TYPE, SCHEMA_ORG.Organization)
				.subject("http://example.org/3")
					.add(RDF.TYPE, SCHEMA_ORG.Place)
					.add(SCHEMA_ORG.latitude, literal(Double.valueOf("4.813279429363683E1")))
					.add(SCHEMA_ORG.longitude, literal(Double.valueOf("1.1676040828077141E1"))) 
				.subject("http://example.org/4")
					.add(RDF.TYPE, SCHEMA_ORG.Thing)
				.build();
		PublicationValidator.validatePublication(model);	
	}
}
