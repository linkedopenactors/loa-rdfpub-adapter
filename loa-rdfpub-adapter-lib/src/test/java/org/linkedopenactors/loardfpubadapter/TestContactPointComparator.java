package org.linkedopenactors.loardfpubadapter;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.StringReader;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.loardfpubadapter.model.Publication;
import org.linkedopenactors.loardfpubadapter.model.PublicationModelsFactory;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;
import lombok.NonNull;

class TestContactPointComparator {

	@Test
	void test() {
		IRI subject = Values.iri("http://example.com");
		Publication latest = PublicationModelsFactory.getPublicationHistoryInstance(getTestPublication(subject).getModel()).orElseThrow().getLatest().orElseThrow();
		assertTrue(ComparatorContactPoint.hasIdenticalContactPoint(latest, latest));
	}

	@Test
	void test2() {
		IRI subject = Values.iri("http://example.com");
		@NonNull
		Model model = getTestPublication(subject).getModel();
		Publication first = PublicationModelsFactory.getPublicationHistoryInstance(model).orElseThrow().getLatest().orElseThrow();
		
		IRI contactPointSubject = first.getOrgansation().getContactPoint().orElseThrow().getId();
		
		Model model2 = new ModelBuilder().build();
		model2.addAll(getTestPublication(subject).getModel());
		model2.remove(contactPointSubject, SCHEMA_ORG.email, null);
		model2.add(contactPointSubject, SCHEMA_ORG.email, Values.literal("test@test.de"));
		model2.add(contactPointSubject, SCHEMA_ORG.name, Values.literal("privat"));
		Publication latest = PublicationModelsFactory.getPublicationHistoryInstance(model2).orElseThrow().getLatest().orElseThrow();
		assertFalse(ComparatorContactPoint.hasIdenticalContactPoint(first, latest));
	}

	private ModelAndSubject getTestPublication(IRI subject) {
		StringReader sr = new StringReader("""
					@prefix kvm: <http://localhost:8081/kvm#> .
					
					<http://example.com> a <https://schema.org/CreativeWork>, <https://www.w3.org/ns/activitystreams#Object>;
					  <https://schema.org/name> "Teikei Gemeinschaft München Trudering";
					  <https://schema.org/creativeWorkStatus> "todo";
					  <https://www.w3.org/ns/activitystreams#name> "Teikei Gemeinschaft München Trudering";
					  <https://schema.org/description> "Verbrauchsgemeinschaft für solidarischen und gesegelten Kaffee. Hier kannst du ökologisch und sozial anspruchsvollen Kaffee bestellen und dich mit anderen Enthusiast*innen von gutem und nachhaltigen Kaffee austauschen. Bestelle schnell und einfach mit Klick auf unsere Homepage.";
					  <https://schema.org/identifier> "9d317daca74246d4be41b1a37e30ee2a";
					  <https://schema.org/license> "CC0-1.0";
					  <https://schema.org/version> "0";
					  <https://schema.org/about> <http://example.com_organisation>;
					  <https://schema.org/keywords> "non-profit", "teikei", "teikei-gemeinschaft";
					  <https://schema.org/dateCreated> "2022-01-29T13:22:41.66";
					  <https://schema.org/dateModified> "2022-03-30T20:18:49.220982407" .
					
					<http://example.com_organisation> a <https://schema.org/Organization>, <https://www.w3.org/ns/activitystreams#Object>;
					  <https://schema.org/name> "Teikei Gemeinschaft München Trudering";
					  <https://schema.org/contactPoint> <http://example.com_contactPoint>;
					  <https://schema.org/location> <http://example.com_place> .
					
					<http://example.com_place> a <https://schema.org/Place>, <https://www.w3.org/ns/activitystreams#Object>;
					  <https://schema.org/latitude> 4.813279429363683E1;
					  <https://schema.org/longitude> 1.1676040828077141E1;
					  <https://schema.org/address> <http://example.com_postalAddress> .
					
					<http://example.com_postalAddress> a <https://schema.org/PostalAddress>, <https://www.w3.org/ns/activitystreams#Object>;
					  <https://schema.org/addressLocality> "München";
					  <https://schema.org/streetAddress> "Linnenbrüggerstrasse 13";
					  <https://schema.org/postalCode> "81829" .
					
					<http://example.com_contactPoint> a <https://schema.org/ContactPoint>, <https://www.w3.org/ns/activitystreams#Object>;
					  <https://schema.org/email> "muenchen-trudering@teikei.community";
					  <https://schema.org/telephone> "" .
									""");
		try {
			return new ModelAndSubject(subject, Rio.parse(sr, RDFFormat.TURTLE));
		} catch (RDFParseException | UnsupportedRDFormatException | IOException e) {
			throw new RuntimeException("error parsing test model", e);
		}
	}
}
