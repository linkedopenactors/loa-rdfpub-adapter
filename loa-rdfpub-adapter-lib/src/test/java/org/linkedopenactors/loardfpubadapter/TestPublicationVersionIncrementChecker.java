package org.linkedopenactors.loardfpubadapter;

import static org.eclipse.rdf4j.model.util.Values.literal;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.loardfpubadapter.model.PublicationHistory;
import org.linkedopenactors.loardfpubadapter.model.PublicationModelsFactory;

import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

class TestPublicationVersionIncrementChecker {

	@Test
	void testNewPublicationWithoutVersion() {
		IRI subject = Values.iri("http://example.org/1"); 
		Model newOne = new ModelBuilder()
				.subject(subject)
					.add(RDF.TYPE, SCHEMA_ORG.CreativeWork)
					.add(SCHEMA_ORG.identifier, literal(815))
				.build();
		Exception exception = assertThrows(IllegalStateException.class, () -> {
			
			PublicationHistory publicationHistory = PublicationModelsFactory.getPublicationHistoryInstance(newOne).orElseThrow();
			assertEquals(1,publicationHistory.versionCount());
			PublicationVersionIncrementChecker.checkOrSet(publicationHistory.getTheOnlyExistingPublication(), null);	
	    });		
		assertEquals("Property https://schema.org/version missing!", exception.getMessage());
	}

	@Test
	void testNewPublicationWithVersion0ButExistingPublication0() {
		testNewPublicationWithWrongVersionButExistingPublication(0,0);
	}
	
	@Test
	void testNewPublicationWithVersion2ButExistingPublication0() {
		testNewPublicationWithWrongVersionButExistingPublication(2,0);
	}
			
	void testNewPublicationWithWrongVersionButExistingPublication(int newVersion, int existingVersion) {
		IRI subject = Values.iri("http://example.org/1"); 
		Model newOne = new ModelBuilder()
				.subject(subject)
					.add(RDF.TYPE, SCHEMA_ORG.CreativeWork)
					.add(SCHEMA_ORG.identifier, literal(815))
					.add(SCHEMA_ORG.version, literal(newVersion))
					.add(SCHEMA_ORG.about, Values.iri("http://example.org/about"))
				.build();
		Model existing = new ModelBuilder()
				.subject(subject)
					.add(RDF.TYPE, SCHEMA_ORG.CreativeWork)
					.add(SCHEMA_ORG.identifier, literal(815))
					.add(SCHEMA_ORG.version, literal(existingVersion))
					.add(SCHEMA_ORG.about, Values.iri("http://example.org/about2"))
				.build();
		Exception exception = assertThrows(IllegalStateException.class, () -> {
			PublicationVersionIncrementChecker.checkOrSet(PublicationModelsFactory.getPublicationHistoryInstance(newOne).orElseThrow().getTheOnlyExistingPublication(), Integer.valueOf(PublicationModelsFactory.getPublicationHistoryInstance(existing).orElseThrow().getTheOnlyExistingPublication().getVersion()));	
	    });		
	    assertEquals("Passed version '"+newVersion+"' has to be "+(existingVersion+1)+", because the newest publication in the store is "+existingVersion, exception.getMessage());
	}
}
