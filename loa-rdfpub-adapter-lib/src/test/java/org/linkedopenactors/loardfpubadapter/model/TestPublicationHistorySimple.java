package org.linkedopenactors.loardfpubadapter.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.StringReader;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.junit.jupiter.api.Test;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.vocabulary.AS;

class TestPublicationHistorySimple {

	private static final String NAME = "Teikei Gemeinschaft München Trudering";
	private static final String EMAIL = "muenchen-trudering@teikei.community";
	private static final String ID = "9d317daca74246d4be41b1a37e30ee2a";
	@Test
	void test() {
		IRI subject = Values.iri("http://example.com");
		PublicationHistory publicationHistory = new PublicationHistorySimple(getTestObject(subject));
		assertEquals(ID,  publicationHistory.getIdentifier().orElseThrow());
		assertEquals(1,  publicationHistory.getPublicationsByVersion().size());
		assertEquals(NAME,  publicationHistory.getLatest().orElseThrow().getOrgansation().getName().orElseThrow());
		assertEquals(EMAIL,  publicationHistory.getLatest().orElseThrow().getOrgansation().getContactPoint().orElseThrow().getEmail().orElseThrow());
	}

		private Model getTestObject(IRI subject) {
			ModelAndSubject testPublication = getTestPublication(subject);
			Model activity = new ModelBuilder()
					.subject("http://example.com/someActivity")
						.add(RDF.TYPE, AS.Create)
						.add(AS.object, testPublication.getSubject())
					.build();
			activity.addAll(testPublication.getModel());
			return activity;
		}
		
		private ModelAndSubject getTestPublication(IRI subject) {
			StringReader sr = new StringReader("""
						@prefix kvm: <http://localhost:8081/kvm#> .
						
						<http://example.com> a <https://schema.org/CreativeWork>, <https://www.w3.org/ns/activitystreams#Object>;
						  <https://schema.org/name> "%s";
						  <https://schema.org/creativeWorkStatus> "todo";
						  <https://www.w3.org/ns/activitystreams#name> "Teikei Gemeinschaft München Trudering";
						  <https://schema.org/description> "Verbrauchsgemeinschaft für solidarischen und gesegelten Kaffee. Hier kannst du ökologisch und sozial anspruchsvollen Kaffee bestellen und dich mit anderen Enthusiast*innen von gutem und nachhaltigen Kaffee austauschen. Bestelle schnell und einfach mit Klick auf unsere Homepage.";
						  <https://schema.org/identifier> "%s";
						  <https://schema.org/license> "CC0-1.0";
						  <https://schema.org/version> "1";
						  <https://schema.org/about> <http://example.com_organisation>;
						  <https://schema.org/keywords> "non-profit", "teikei", "teikei-gemeinschaft";
						  <https://schema.org/dateCreated> "2022-01-29T13:22:41.66";
						  <https://schema.org/dateModified> "2022-03-30T20:18:49.220982407" .
						
						<http://example.com_organisation> a <https://schema.org/Organization>, <https://www.w3.org/ns/activitystreams#Object>;
						  <https://schema.org/name> "Teikei Gemeinschaft München Trudering";
						  <https://schema.org/contactPoint> <http://example.com_contactPoint>;
						  <https://schema.org/location> <http://example.com_place> .
						
						<http://example.com_place> a <https://schema.org/Place>, <https://www.w3.org/ns/activitystreams#Object>;
						  <https://schema.org/latitude> 4.813279429363683E1;
						  <https://schema.org/longitude> 1.1676040828077141E1;
						  <https://schema.org/address> <http://example.com_postalAddress> .
						
						<http://example.com_postalAddress> a <https://schema.org/PostalAddress>, <https://www.w3.org/ns/activitystreams#Object>;
						  <https://schema.org/addressLocality> "München";
						  <https://schema.org/streetAddress> "Linnenbrüggerstrasse 13";
						  <https://schema.org/postalCode> "81829" .
						
						<http://example.com_contactPoint> a <https://schema.org/ContactPoint>, <https://www.w3.org/ns/activitystreams#Object>;
						  <https://schema.org/email> "%s";
						  <https://schema.org/telephone> "" .
										""".formatted(NAME, ID, EMAIL));
			try {
				return new ModelAndSubject(subject, Rio.parse(sr, RDFFormat.TURTLE));
			} catch (RDFParseException | UnsupportedRDFormatException | IOException e) {
				throw new RuntimeException("error parsing test model", e);
			}
		}
}
