package org.linkedopenactors.loardfpubadapter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

class TestNextPrevProcessing {

	@org.junit.jupiter.api.Test
	void test() {
		Map<Long, String> test = new HashMap<>();
		test.put(0L, "test0");
		test.put(1L, "test1");
		test.put(2L, "test2");
		test.put(3L, "test3");
		
		NextPrevProcessing<String> s = new NextPrevProcessing<>(test);
		
		assertEquals(Optional.empty(), s.getNext(4));
		assertEquals(Optional.empty(), s.getNext(3));
		assertEquals(Optional.of(test.get(3L)), s.getNext(2));
		assertEquals(Optional.of(test.get(2L)), s.getNext(1));
		assertEquals(Optional.of(test.get(1L)), s.getNext(0));
		assertEquals(Optional.empty(), s.getNext(-1));
		assertEquals(Optional.empty(), s.getNext(-2));
				
		assertEquals(Optional.empty(), s.getPrev(5));
		assertEquals(Optional.empty(), s.getPrev(4));
		assertEquals(Optional.of(test.get(2L)), s.getPrev(3));
		assertEquals(Optional.of(test.get(1L)), s.getPrev(2));
		assertEquals(Optional.of(test.get(0L)), s.getPrev(1));
		assertEquals(Optional.empty(), s.getPrev(0));
		assertEquals(Optional.empty(), s.getPrev(-1));
		assertEquals(Optional.empty(), s.getPrev(-2));
	}
	
	@org.junit.jupiter.api.Test
	void testNullPointer() {
		Map<Long, String> test = null;
		
		NextPrevProcessing<String> s = new NextPrevProcessing<>(test);
		
		assertEquals(Optional.empty(), s.getNext(2));
		assertEquals(Optional.empty(), s.getNext(1));
		assertEquals(Optional.empty(), s.getNext(0));
		assertEquals(Optional.empty(), s.getNext(-1));
		assertEquals(Optional.empty(), s.getNext(-2));
				
		assertEquals(Optional.empty(), s.getPrev(2));
		assertEquals(Optional.empty(), s.getPrev(1));
		assertEquals(Optional.empty(), s.getPrev(0));
		assertEquals(Optional.empty(), s.getPrev(-1));
		assertEquals(Optional.empty(), s.getPrev(-2));
	}

	@org.junit.jupiter.api.Test
	void testEmptyMap() {
		Map<Long, String> test = new HashMap<>();
		
		NextPrevProcessing<String> s = new NextPrevProcessing<>(test);
		
		assertEquals(Optional.empty(), s.getNext(2));
		assertEquals(Optional.empty(), s.getNext(1));
		assertEquals(Optional.empty(), s.getNext(0));
		assertEquals(Optional.empty(), s.getNext(-1));
		assertEquals(Optional.empty(), s.getNext(-2));
				
		assertEquals(Optional.empty(), s.getPrev(2));
		assertEquals(Optional.empty(), s.getPrev(1));
		assertEquals(Optional.empty(), s.getPrev(0));
		assertEquals(Optional.empty(), s.getPrev(-1));
		assertEquals(Optional.empty(), s.getPrev(-2));
	}

}
