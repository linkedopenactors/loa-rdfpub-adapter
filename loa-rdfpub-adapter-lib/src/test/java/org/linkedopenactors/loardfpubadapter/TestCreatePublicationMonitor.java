package org.linkedopenactors.loardfpubadapter;

import static org.eclipse.rdf4j.model.util.Values.literal;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.io.IOException;
import java.io.StringReader;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.loardfpubadapter.model.Publication;
import org.linkedopenactors.loardfpubadapter.model.PublicationHistory;
import org.linkedopenactors.loardfpubadapter.model.PublicationModelsFactory;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

public class TestCreatePublicationMonitor {
	
	@Test
	void testWithoutChanges() {
		// Prepare the first Publication
		Publication firstPublication = prepareFirstPublication();
		
		// Prepare the second Publication
		IRI secondPublicationSubject = Values.iri("http://example.com/second");
		Model secondPublicationModel = getTestPublication(secondPublicationSubject).getModel();		
		secondPublicationModel.remove(secondPublicationSubject, SCHEMA_ORG.version, null);
		secondPublicationModel.add(secondPublicationSubject, SCHEMA_ORG.version, Values.literal(1));		

		// Prepare the PublicationHistory		
		PublicationHistory publicationHistory = preparePublicationHistory(firstPublication, secondPublicationModel);
		
		// call the objectToTest
		Publication publication = callObjectToTest(publicationHistory);
		
		// validatie the result
		
		// the link to the ContactPoint stays the same!
		assertEquals(firstPublication.getOrgansation().getContactPointSubject(), publication.getOrgansation().getContactPointSubject());
		
		// the link to the Location(Place) stays the same!
		assertEquals(firstPublication.getOrgansation().getLocationSubject(), publication.getOrgansation().getLocationSubject());
	}

	private PublicationHistory preparePublicationHistory(Publication firstPublication, Model secondPublicationModel) {
		Model modelOfBoth = new ModelBuilder().build();
		modelOfBoth.addAll(firstPublication.getModel());
		modelOfBoth.addAll(secondPublicationModel);		
		PublicationHistory publicationHistory = PublicationModelsFactory.getPublicationHistoryInstance(modelOfBoth).orElseThrow();
		return publicationHistory;
	}

	@Test
	void testSameLocationButDifferentPostalAddress() {
		// Prepare the first Publication
		Publication firstPublication = prepareFirstPublication();
		
		// Prepare the second Publication
		IRI secondPublicationSubject = Values.iri("http://example.com/second");
		Model secondPublicationModel = getTestPublication(secondPublicationSubject).getModel();		
		secondPublicationModel.remove(secondPublicationSubject, SCHEMA_ORG.version, null);
		secondPublicationModel.add(secondPublicationSubject, SCHEMA_ORG.version, Values.literal(1));		

		Publication secondPublication = PublicationModelsFactory.getPublicationHistoryInstance(secondPublicationModel).orElseThrow().getTheOnlyExistingPublication();

		IRI postalAddressSubject = secondPublication.getOrgansation().getLocation().orElseThrow().getPostalAddress().orElseThrow().getId();
		secondPublicationModel.remove(postalAddressSubject, SCHEMA_ORG.postalCode, null);

		// Prepare the PublicationHistory
		PublicationHistory publicationHistory = preparePublicationHistory(firstPublication, secondPublicationModel);
		
		// call the objectToTest
		Publication publication = callObjectToTest(publicationHistory);

		// validatie the result
		
		// the link to the ContactPoint stays the same!
		assertEquals(firstPublication.getOrgansation().getContactPointSubject(), publication.getOrgansation().getContactPointSubject());
		
		// the link to the Location(Place) stays NOT the same!
		assertNotEquals(firstPublication.getOrgansation().getLocationSubject(), publication.getOrgansation().getLocationSubject());
		
		// the link to the PostalAddress stays NOT the same!
		assertNotEquals(firstPublication.getOrgansation().getLocation().orElseThrow().getPostalAddressSubject(), publication.getOrgansation().getLocation().orElseThrow().getPostalAddressSubject());
	}

	@Test
	void testOnlyOrganisationChanged() {
		// Prepare the first Publication
		Publication firstPublication = prepareFirstPublication();
		
		// Prepare the second Publication
		IRI secondPublicationSubject = Values.iri("http://example.com/second");
		Model secondPublicationModel = getTestPublication(secondPublicationSubject).getModel();		
		secondPublicationModel.remove(secondPublicationSubject, SCHEMA_ORG.version, null);
		secondPublicationModel.add(secondPublicationSubject, SCHEMA_ORG.version, Values.literal(1));		

		Publication secondPublication = PublicationModelsFactory.getPublicationHistoryInstance(secondPublicationModel).orElseThrow().getTheOnlyExistingPublication();

		IRI organsationSubject = secondPublication.getOrgansation().getId();
		secondPublicationModel.remove(organsationSubject, SCHEMA_ORG.name, null);
		secondPublicationModel.add(organsationSubject, SCHEMA_ORG.name, literal("Kein Name mehr !!"));

		// Prepare the PublicationHistory
		PublicationHistory publicationHistory = preparePublicationHistory(firstPublication, secondPublicationModel);
		
		// call the objectToTest
		Publication publication = callObjectToTest(publicationHistory);

		// validatie the result
		
		// the link to the ContactPoint stays the same!
		assertEquals(firstPublication.getOrgansation().getContactPointSubject(), publication.getOrgansation().getContactPointSubject());
		
		// the link to the Location(Place) stays the same!
		assertEquals(firstPublication.getOrgansation().getLocationSubject(), publication.getOrgansation().getLocationSubject());
	}

	@Test
	void testOnlyOrganisationPlaceAndContactPointChanged() {
		// Prepare the first Publication
		Publication firstPublication = prepareFirstPublication();
		
		// Prepare the second Publication
		IRI secondPublicationSubject = Values.iri("http://example.com/second");
		Model secondPublicationModel = getTestPublication(secondPublicationSubject).getModel();		
		secondPublicationModel.remove(secondPublicationSubject, SCHEMA_ORG.version, null);
		secondPublicationModel.add(secondPublicationSubject, SCHEMA_ORG.version, Values.literal(1));		

		Publication secondPublication = PublicationModelsFactory.getPublicationHistoryInstance(secondPublicationModel).orElseThrow().getTheOnlyExistingPublication();

		IRI organsationSubject = secondPublication.getOrgansation().getId();
		secondPublicationModel.remove(organsationSubject, SCHEMA_ORG.name, null);

		IRI contactPointSubject = secondPublication.getOrgansation().getContactPoint().orElseThrow().getId();		
		secondPublicationModel.remove(contactPointSubject, SCHEMA_ORG.email, null);
		secondPublicationModel.add(contactPointSubject, SCHEMA_ORG.email, Values.literal("test@test.de"));
		secondPublicationModel.add(contactPointSubject, SCHEMA_ORG.name, Values.literal("privat"));

		IRI locationSubject = secondPublication.getOrgansation().getLocation().orElseThrow().getId();
		secondPublicationModel.remove(locationSubject, SCHEMA_ORG.latitude, null);
		secondPublicationModel.add(locationSubject, SCHEMA_ORG.latitude, Values.literal(2));

		// Prepare the PublicationHistory
		PublicationHistory publicationHistory = preparePublicationHistory(firstPublication, secondPublicationModel);
		
		// call the objectToTest
		Publication publication = callObjectToTest(publicationHistory);
		// validatie the result

		// the link to the Location(Place) stays NOT the same!
		assertNotEquals(firstPublication.getOrgansation().getId(), publication.getOrgansation().getId());

		// the link to the ContactPoint stays NOT the same!
		assertNotEquals(firstPublication.getOrgansation().getContactPointSubject(), publication.getOrgansation().getContactPointSubject());

		// the link to the Location(Place) stays NOT the same!
		assertNotEquals(firstPublication.getOrgansation().getLocationSubject(), publication.getOrgansation().getLocationSubject());
	}

	@Test
	void testWithChangedContactPointAndLocation() {
		// Prepare the first Publication
		Publication firstPublication = prepareFirstPublication();
		
		// Prepare the second Publication
		IRI secondPublicationSubject = Values.iri("http://example.com/second");
		Model secondPublicationModel = getTestPublication(secondPublicationSubject).getModel();		
		secondPublicationModel.remove(secondPublicationSubject, SCHEMA_ORG.version, null);
		secondPublicationModel.add(secondPublicationSubject, SCHEMA_ORG.version, Values.literal(1));		
		
		Publication secondPublication = PublicationModelsFactory.getPublicationHistoryInstance(secondPublicationModel).orElseThrow().getTheOnlyExistingPublication();

		IRI contactPointSubject = secondPublication.getOrgansation().getContactPoint().orElseThrow().getId();		
		secondPublicationModel.remove(contactPointSubject, SCHEMA_ORG.email, null);
		secondPublicationModel.add(contactPointSubject, SCHEMA_ORG.email, Values.literal("test@test.de"));
		secondPublicationModel.add(contactPointSubject, SCHEMA_ORG.name, Values.literal("privat"));
		
		IRI locationSubject = secondPublication.getOrgansation().getLocation().orElseThrow().getId();
		secondPublicationModel.remove(locationSubject, SCHEMA_ORG.latitude, null);
		secondPublicationModel.add(locationSubject, SCHEMA_ORG.latitude, Values.literal(2));

		// Prepare the PublicationHistory
		PublicationHistory publicationHistory = preparePublicationHistory(firstPublication, secondPublicationModel);
		
		// call the objectToTest
		Publication publication = callObjectToTest(publicationHistory);
		
		// validatie the result
		
		// the link to the ContactPoint stays NOT the same!
		assertNotEquals(firstPublication.getOrgansation().getContactPointSubject(), publication.getOrgansation().getContactPointSubject());
		
		// the link to the Location(Place) stays NOT the same!
		assertNotEquals(firstPublication.getOrgansation().getLocationSubject(), publication.getOrgansation().getLocationSubject());
		
		// the link to the PostalAddress stays NOT the same!
		assertEquals(firstPublication.getOrgansation().getLocation().orElseThrow().getPostalAddressSubject(), publication.getOrgansation().getLocation().orElseThrow().getPostalAddressSubject());
	}

	@Test
	void testWithSimpleChanges() {
		// Prepare the first Publication
		Publication firstPublication = prepareFirstPublication();
		
		// Prepare the second Publication
		IRI secondPublicationSubject = Values.iri("http://example.com/latest");
		Model secondPublicationModel = getTestPublication(secondPublicationSubject).getModel();		
		secondPublicationModel.remove(secondPublicationSubject, SCHEMA_ORG.version, null);
		secondPublicationModel.add(secondPublicationSubject, SCHEMA_ORG.version, Values.literal(1));		
		
		Publication secondPublication = PublicationModelsFactory.getPublicationHistoryInstance(secondPublicationModel).orElseThrow().getTheOnlyExistingPublication();

		IRI contactPointSubject = secondPublication.getOrgansation().getContactPoint().orElseThrow().getId();		
		secondPublicationModel.remove(contactPointSubject, SCHEMA_ORG.email, null);
		secondPublicationModel.add(contactPointSubject, SCHEMA_ORG.email, Values.literal("test@test.de"));
		secondPublicationModel.add(contactPointSubject, SCHEMA_ORG.name, Values.literal("privat"));
		
		// Prepare the PublicationHistory
		PublicationHistory publicationHistory = preparePublicationHistory(firstPublication, secondPublicationModel);
		
		// call the objectToTest
		Publication publication = callObjectToTest(publicationHistory);
		
		// validatie the result

		publication.debugDump("publicationd");
		// the link to the ContactPoint stays NOT the same!
		assertNotEquals(firstPublication.getOrgansation().getContactPointSubject(), publication.getOrgansation().getContactPointSubject());
	}

	private Publication callObjectToTest(PublicationHistory publicationHistory) {
		CreatePublicationMonitor objectToTest = new CreatePublicationMonitor ();		
		Publication publicationToCreate = publicationHistory.getLatest().orElseThrow();
		Publication existingPublication = publicationHistory.previousPublication(publicationToCreate.getVersion()).orElseThrow();		
		
		Publication publication = objectToTest.monitorCreation(publicationToCreate, existingPublication);
		return publication;
	}

	private Publication prepareFirstPublication() {
		Model firstPublicationModel = getTestPublication(Values.iri("http://example.com/first")).getModel();
		Publication firstPublication = PublicationModelsFactory.getPublicationHistoryInstance(firstPublicationModel).orElseThrow().getTheOnlyExistingPublication();
		return firstPublication;
	}
	
	private ModelAndSubject getTestPublication(IRI subject) {
		StringReader sr = new StringReader("""
					@prefix kvm: <http://localhost:8081/kvm#> .
					
					<%s> a <https://schema.org/CreativeWork>, <https://www.w3.org/ns/activitystreams#Object>;
					  <https://schema.org/name> "Teikei Gemeinschaft München Trudering";
					  <https://schema.org/creativeWorkStatus> "todo";
					  <https://www.w3.org/ns/activitystreams#name> "Teikei Gemeinschaft München Trudering";
					  <https://schema.org/description> "Verbrauchsgemeinschaft für solidarischen und gesegelten Kaffee. Hier kannst du ökologisch und sozial anspruchsvollen Kaffee bestellen und dich mit anderen Enthusiast*innen von gutem und nachhaltigen Kaffee austauschen. Bestelle schnell und einfach mit Klick auf unsere Homepage.";
					  <https://schema.org/identifier> "9d317daca74246d4be41b1a37e30ee2a";
					  <https://schema.org/license> "CC0-1.0";
					  <https://schema.org/version> "0";
					  <https://schema.org/about> <%s_organisation>;
					  <https://schema.org/keywords> "non-profit", "teikei", "teikei-gemeinschaft";
					  <https://schema.org/dateCreated> "2022-01-29T13:22:41.66";
					  <https://schema.org/dateModified> "2022-03-30T20:18:49.220982407" .
					
					<%s_organisation> a <https://schema.org/Organization>, <https://www.w3.org/ns/activitystreams#Object>;
					  <https://schema.org/name> "Teikei Gemeinschaft München Trudering";
					  <https://schema.org/contactPoint> <%s_contactPoint>;
					  <https://schema.org/location> <%s_place> .
					
					<%s_place> a <https://schema.org/Place>, <https://www.w3.org/ns/activitystreams#Object>;
					  <https://schema.org/latitude> 4.813279429363683E1;
					  <https://schema.org/longitude> 1.1676040828077141E1;
					  <https://schema.org/address> <%s_postalAddress> .
					
					<%s_postalAddress> a <https://schema.org/PostalAddress>, <https://www.w3.org/ns/activitystreams#Object>;
					  <https://schema.org/addressLocality> "München";
					  <https://schema.org/streetAddress> "Linnenbrüggerstrasse 13";
					  <https://schema.org/postalCode> "81829" .
					
					<%s_contactPoint> a <https://schema.org/ContactPoint>, <https://www.w3.org/ns/activitystreams#Object>;
					  <https://schema.org/email> "muenchen-trudering@teikei.community";
					  <https://schema.org/telephone> "" .
									""".formatted(subject, subject, subject, subject, subject, subject, subject, subject, subject));
		try {
			return new ModelAndSubject(subject, Rio.parse(sr, RDFFormat.TURTLE));
		} catch (RDFParseException | UnsupportedRDFormatException | IOException e) {
			throw new RuntimeException("error parsing test model", e);
		}
	}
}
