package org.linkedopenactors.loardfpubadapter;

import java.util.Optional;
import java.util.function.Function;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.loardfpubadapter.model.PublicationHistory;
import org.springframework.core.io.ClassPathResource;

class TestPublicationHistoryConstructor
{
	@Test
	void testSavePublicationsWithDifferentVersions() throws Exception {

		String identifier = "0884c4e86e404072b6874b99b7e32640";

		ClassPathResource cpr1 = new ClassPathResource(identifier + ".ttl");
		ClassPathResource cpr2 = new ClassPathResource("86bc6dd41091435795aad552db94556e.ttl");
		ClassPathResource cpr3 = new ClassPathResource("df2f726a287f439cbc59b72a1c738921.ttl");

		IRI userid = org.eclipse.rdf4j.model.util.Values.iri("http://example.com/user");
		Repository repo = new SailRepository(new MemoryStore());
		try (RepositoryConnection con = repo.getConnection()) {
			con.add(cpr1.getInputStream(), RDFFormat.TURTLE, userid);
			con.add(cpr2.getInputStream(), RDFFormat.TURTLE, userid);
			con.add(cpr3.getInputStream(), RDFFormat.TURTLE, userid);
		}

		PublicationHistoryConstructor constructor = new PublicationHistoryConstructor();
		Function<String, Model> queryable = query -> Repositories.graphQuery(repo, query, r -> QueryResults.asModel(r));
		Optional<PublicationHistory> publicationHistoryOpt = constructor.getConstructedPublication(queryable, userid, identifier);
		
		publicationHistoryOpt.orElseThrow().dump("publicationHistory");
	}
}
