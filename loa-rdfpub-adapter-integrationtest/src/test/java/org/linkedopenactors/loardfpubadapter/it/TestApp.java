package org.linkedopenactors.loardfpubadapter.it;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@SpringBootApplication(scanBasePackages = {
		"org.linkedopenactors.rdfpub.client"
		})
@Configuration
@Slf4j
public class TestApp {
	
	public static void main(String[] args) {
		SpringApplication.run(TestApp.class, args);
	}
	
	@Bean
	public WebClient.Builder webClientBuilder() {
		return WebClient.builder();
	}
	
	@Bean
	public WebClient getWebClient() {
		return webClientBuilder()
				.filters(exchangeFilterFunctions -> {
				      exchangeFilterFunctions.add(logRequest());
				      exchangeFilterFunctions.add(logResponse());
				      exchangeFilterFunctions.add(filterFunction);				      
				  })
				.build();
	}
	
	ExchangeFilterFunction logRequest() {
	    return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {	    	
	        if (log.isDebugEnabled()) {
	            StringBuilder sb = new StringBuilder("Request: \n");
	            sb.append(clientRequest.method().name()).append(" - ").append(clientRequest.url()).append("\n");
	            clientRequest
	              .headers()
	              .forEach((name, values) -> values.forEach(value -> sb.append(name).append(" - ").append(value).append("\n")));
	            log.debug(sb.toString());
	        }
	        return Mono.just(clientRequest);
	    });
	}	

	ExchangeFilterFunction logResponse() {
	    return ExchangeFilterFunction.ofResponseProcessor(clientResponse -> {
	        if (log.isDebugEnabled()) {
	            StringBuilder sb = new StringBuilder("Response: \n");
	            sb.append("status: " + clientResponse.statusCode()).append("\n");	            
	            clientResponse
	              .headers().asHttpHeaders()
	              .forEach((name, values) -> values.forEach(value -> sb.append(name).append(" - ").append(value).append("\n")));
	            log.debug(sb.toString());
	            
	        }
	        return Mono.just(clientResponse);
	    });
	}	

	ExchangeFilterFunction filterFunction = (clientRequest, nextFilter) -> {
	    log.info("WebClient fitler executed");
	    return nextFilter.exchange(clientRequest);
	};
}
