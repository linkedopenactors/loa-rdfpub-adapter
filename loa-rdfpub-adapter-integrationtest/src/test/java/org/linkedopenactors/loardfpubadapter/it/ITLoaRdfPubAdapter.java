package org.linkedopenactors.loardfpubadapter.it;

import static org.eclipse.rdf4j.model.util.Values.literal;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.io.StringReader;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.linkedopenactors.loardfpubadapter.LoaRdfPubAdapter;
import org.linkedopenactors.loardfpubadapter.model.ContactPoint;
import org.linkedopenactors.loardfpubadapter.model.Place;
import org.linkedopenactors.loardfpubadapter.model.Publication;
import org.linkedopenactors.loardfpubadapter.model.PublicationHistory;
import org.linkedopenactors.rdfpub.client.AccessTokenManager;
import org.linkedopenactors.rdfpub.client.RdfPubClient;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.vocabulary.AS;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class ITLoaRdfPubAdapter extends ITBase {

	// TODO reactivate ??
//	@Test
	void testSavePublicationsWithDifferentVersions() throws Exception {
		log.debug("##################################");
		log.debug(this.getClass().getName());
		log.debug("##################################");
		IRI subject = Values.iri("http://example.com");		
		TestUser randomTestUser = getRandomTestUser();
		RdfPubClient rdfPubClient4RandomTestUser = getRdfPubClient(randomTestUser);
		AccessTokenManager accessTokenManager4RandomTestUser = getAccessTokenManager(randomTestUser);
		LoaRdfPubAdapter loaRdfPubAdapter4RandomTestUser = new LoaRdfPubAdapter(rdfPubClient4RandomTestUser);

		String identifier = "9d317daca74246d4be41b1a37e30ee2a";
		Model testObject = getTestObject(subject, identifier);
		testObject.add(subject, SCHEMA_ORG.version, literal(0));
		
		// Create Publication
		IRI activityIri = loaRdfPubAdapter4RandomTestUser.createPublication(testObject, accessTokenManager4RandomTestUser.getAuthToken());
		PublicationHistory publicationHistory = loaRdfPubAdapter4RandomTestUser.getPublicationOfActivity(activityIri, accessTokenManager4RandomTestUser.getAuthToken()).orElseThrow();
		publicationHistory.dump("publicationHistory:");
		assertEquals("Teikei Gemeinschaft München Trudering", publicationHistory.getLatest().orElseThrow().getOrgansation().getName().orElseThrow());

		// save second
		Exception exception = assertThrows(IllegalStateException.class, () -> {
			loaRdfPubAdapter4RandomTestUser.createPublication(getTestObject(subject,identifier), accessTokenManager4RandomTestUser.getAuthToken());	
	    });		
	    assertEquals("Property https://schema.org/version missing!", exception.getMessage());
	    
		// save again
		exception = assertThrows(IllegalStateException.class, () -> {
			Model objectToTest = getTestObject(subject, identifier);
			objectToTest.add(subject, SCHEMA_ORG.version, literal(2));
			loaRdfPubAdapter4RandomTestUser.createPublication(objectToTest, accessTokenManager4RandomTestUser.getAuthToken());	
	    });		
	    assertEquals("Passed version '2' has to be 1, because the newest publication in the store is 0", exception.getMessage());	    

		// save again
		testObject = getTestObject(subject, identifier);
		testObject.add(subject, SCHEMA_ORG.version, literal(1));
		activityIri = loaRdfPubAdapter4RandomTestUser.createPublication(testObject, accessTokenManager4RandomTestUser.getAuthToken());	

		publicationHistory = loaRdfPubAdapter4RandomTestUser.getPublicationOfActivity(activityIri, accessTokenManager4RandomTestUser.getAuthToken()).orElseThrow();
		Integer latestVersion = publicationHistory.getLatest().orElseThrow().getVersion();
		assertEquals(1, latestVersion);
		assertEquals(0, publicationHistory.previousPublication(latestVersion).orElseThrow().getVersion());
	}

//	@Test
	void testThatUnchangedPlaceIsReused() throws Exception {
		log.debug("##################################");
		log.debug(this.getClass().getName());
		log.debug("##################################");
		IRI subject = Values.iri("http://example.com");		
		TestUser randomTestUser = getRandomTestUser();
		RdfPubClient rdfPubClient4RandomTestUser = getRdfPubClient(randomTestUser);
		LoaRdfPubAdapter loaRdfPubAdapter4RandomTestUser = new LoaRdfPubAdapter(rdfPubClient4RandomTestUser);
		AccessTokenManager accessTokenManager4RandomTestUser = getAccessTokenManager(randomTestUser);

		String id = "9d317daca74246d4be41b1a37e30ee2b";
		Model testObject = getTestObject(subject, id);
		testObject.add(subject, SCHEMA_ORG.version, literal(0));
		IRI activityIriV0 = loaRdfPubAdapter4RandomTestUser.createPublication(testObject, accessTokenManager4RandomTestUser.getAuthToken());
		
		// save a second version
		testObject = getTestObject(subject, id);
		testObject.add(subject, SCHEMA_ORG.version, literal(1));
		IRI activityIriV1 = loaRdfPubAdapter4RandomTestUser.createPublication(testObject, accessTokenManager4RandomTestUser.getAuthToken());	

		PublicationHistory publicationHistory0 = loaRdfPubAdapter4RandomTestUser.getPublicationOfActivity(activityIriV0, accessTokenManager4RandomTestUser.getAuthToken()).orElseThrow();
		PublicationHistory publicationHistory1 = loaRdfPubAdapter4RandomTestUser.getPublicationOfActivity(activityIriV1, accessTokenManager4RandomTestUser.getAuthToken()).orElseThrow();
		
		// check, that both publications are saved and the latest is the same version 
		assertEquals(2, publicationHistory0.versionCount());
		Publication publicationLatest = publicationHistory0.getLatest().orElseThrow();
		assertEquals(publicationLatest.getVersion(), publicationHistory1.getLatest().orElseThrow().getVersion());
		
		Publication firstPublication = publicationHistory0.previousPublication(publicationLatest.getVersion()).orElseThrow();

		assertEquals(0, firstPublication.getVersion());
		assertEquals(1, publicationLatest.getVersion());
		
		Place latestPlace = publicationLatest.getOrgansation().getLocation().orElseThrow();
		Place firstPlace = firstPublication.getOrgansation().getLocation().orElseThrow();
		
		// check, that the place isn't saved twice ! Means the Organisation links to the same place object iri.
		assertEquals(latestPlace.getId(), firstPlace.getId());
	}
	
//	@Test
	void testThatUnchangedContactPointIsReused() throws Exception {
		log.debug("##################################");
		log.debug(this.getClass().getName());
		log.debug("##################################");
		IRI subject = Values.iri("http://example.com");		
		TestUser randomTestUser = getRandomTestUser();
		RdfPubClient rdfPubClient4RandomTestUser = getRdfPubClient(randomTestUser);
		LoaRdfPubAdapter loaRdfPubAdapter4RandomTestUser = new LoaRdfPubAdapter(rdfPubClient4RandomTestUser);
		AccessTokenManager accessTokenManager4RandomTestUser = getAccessTokenManager(randomTestUser);
		
		Model testObject = getTestObject(subject, "9d317daca74246d4be41b1a37e30ee2x");
		testObject.add(subject, SCHEMA_ORG.version, literal(0));
		IRI activityIri = loaRdfPubAdapter4RandomTestUser.createPublication(testObject, accessTokenManager4RandomTestUser.getAuthToken());
		
		// save a second version
		testObject = getTestObject(subject, "9d317daca74246d4be41b1a37e30ee2x");
		testObject.add(subject, SCHEMA_ORG.version, literal(1));
		loaRdfPubAdapter4RandomTestUser.createPublication(testObject, accessTokenManager4RandomTestUser.getAuthToken());	

		PublicationHistory publicationHistory = loaRdfPubAdapter4RandomTestUser.getPublicationOfActivity(activityIri, accessTokenManager4RandomTestUser.getAuthToken()).orElseThrow();
		Publication publicationLatest = publicationHistory.getLatest().orElseThrow();
		Publication firstPublication = publicationHistory.previousPublication(publicationLatest.getVersion()).orElseThrow();
		ContactPoint latestContactPoint = publicationLatest.getOrgansation().getContactPoint().orElseThrow();
		ContactPoint firstContactPoint = firstPublication.getOrgansation().getContactPoint().orElseThrow();
		
		// check, that the ContactPoint isn't saved twice ! Means the Organisation links to the same ContactPoint object iri.
		assertEquals(latestContactPoint.getId(), firstContactPoint.getId());
	}

	private Model getTestObject(IRI subject, String id) {
		ModelAndSubject testPublication = getTestPublication(subject, id);
		Model activity = new ModelBuilder()
				.subject("http://example.com/someActivity")
					.add(RDF.TYPE, AS.Create)
					.add(AS.object, testPublication.getSubject())
				.build();
		activity.addAll(testPublication.getModel());
		return activity;
	}
	
	private ModelAndSubject getTestPublication(IRI subject, String id) {
		StringReader sr = new StringReader("""
					@prefix kvm: <http://localhost:8081/kvm#> .
					
					<http://example.com> a <https://schema.org/CreativeWork>, <https://www.w3.org/ns/activitystreams#Object>;
					  <https://schema.org/name> "Teikei Gemeinschaft München Trudering";
					  <https://schema.org/creativeWorkStatus> "todo";
					  <https://www.w3.org/ns/activitystreams#name> "Teikei Gemeinschaft München Trudering";
					  <https://schema.org/description> "Verbrauchsgemeinschaft für solidarischen und gesegelten Kaffee. Hier kannst du ökologisch und sozial anspruchsvollen Kaffee bestellen und dich mit anderen Enthusiast*innen von gutem und nachhaltigen Kaffee austauschen. Bestelle schnell und einfach mit Klick auf unsere Homepage.";
					  <https://schema.org/identifier> "%s";
					  <https://schema.org/license> "CC0-1.0";
					  <https://schema.org/about> <http://example.com_organisation>;
					  <https://schema.org/keywords> "non-profit", "teikei", "teikei-gemeinschaft";
					  <https://schema.org/dateCreated> "2022-01-29T13:22:41.66";
					  <https://schema.org/dateModified> "2022-03-30T20:18:49.220982407" .
					
					<http://example.com_organisation> a <https://schema.org/Organization>, <https://www.w3.org/ns/activitystreams#Object>;
					  <https://schema.org/name> "Teikei Gemeinschaft München Trudering";
					  <https://schema.org/contactPoint> <http://example.com_contactPoint>;
					  <https://schema.org/location> <http://example.com_place> .
					
					<http://example.com_place> a <https://schema.org/Place>, <https://www.w3.org/ns/activitystreams#Object>;
					  <https://schema.org/latitude> 4.813279429363683E1;
					  <https://schema.org/longitude> 1.1676040828077141E1;
					  <https://schema.org/address> <http://example.com_postalAddress> .
					
					<http://example.com_postalAddress> a <https://schema.org/PostalAddress>, <https://www.w3.org/ns/activitystreams#Object>;
					  <https://schema.org/addressLocality> "München";
					  <https://schema.org/streetAddress> "Linnenbrüggerstrasse 13";
					  <https://schema.org/postalCode> "81829" .
					
					<http://example.com_contactPoint> a <https://schema.org/ContactPoint>, <https://www.w3.org/ns/activitystreams#Object>;
					  <https://schema.org/email> "muenchen-trudering@teikei.community";
					  <https://schema.org/telephone> "" .
									""".formatted(id));
		try {
			return new ModelAndSubject(subject, Rio.parse(sr, RDFFormat.TURTLE));
		} catch (RDFParseException | UnsupportedRDFormatException | IOException e) {
			throw new RuntimeException("error parsing test model", e);
		}
	}
}
